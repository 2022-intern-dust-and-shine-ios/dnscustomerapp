//
//  SessionManager.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 1/31/22.
//

import Foundation
import UIKit


extension UserDefaults {
    
    //MARK: - Token
    func setToken(value: String?) {
        setValue(value, forKey: UserDefaultKeys.token.id)
    }
    func getToken() -> String? {
        return object(forKey: UserDefaultKeys.token.id) as? String
    }
    
    //MARK: - Password
    func setPassword(value: String) {
        setValue(value, forKey: UserDefaultKeys.password.id)
    }
    func getPassword() -> String {
        return object(forKey: UserDefaultKeys.password.id) as! String
    }
    
    //MARK: - Company
    func setCompanyId(value: Int) {
        setValue(value, forKey: UserDefaultKeys.company.id)
    }
    func getCompanyId() -> Int {
        return object(forKey: UserDefaultKeys.company.id) as! Int
    }

    func setCompanyName(value: String) {
        setValue(value, forKey: UserDefaultKeys.companyname.id)
    }
    func getCompanyName() -> String {
        return object(forKey: UserDefaultKeys.companyname.id) as! String
    }
    func setCompanyImage(value: String) {
        setValue(value, forKey: UserDefaultKeys.company_image.id)
    }
    func getCompanyImage() -> String {
        return object(forKey: UserDefaultKeys.company_image.id) as! String
    }
    func setCompanyRating(value: Double) {
        setValue(value, forKey: UserDefaultKeys.rating.id)
    }
    func getCompanyRating() -> Double? {
        return object(forKey: UserDefaultKeys.rating.id) as? Double
    }
    //MARK: - Address
    func setAddress(value: String) {
        setValue(value, forKey: UserDefaultKeys.address.id)
    }
    func getAddress() -> String? {
        return object(forKey: UserDefaultKeys.address.id) as? String
    }
    
    //MARK: - Date time
    func setDateTime(value: String) {
        setValue(value, forKey: UserDefaultKeys.datetime.id)
    }
    func getDateTime() -> String {
        return object(forKey: UserDefaultKeys.datetime.id) as! String
    }
    
    //MARK: - Service
    func setServices(value: [Int:Int]) {
        guard let dataDict = try? NSKeyedArchiver.archivedData(withRootObject: value,
                                                               requiringSecureCoding: true) else { return }
        setValue(dataDict, forKey: UserDefaultKeys.services.id)
    }

    func getServices() -> [Int:Int] {
        //return object(forKey: UserDefaultKeys.services.id) as! [Int:Int]
        let data = object(forKey: UserDefaultKeys.services.id) as! Data
        //let output = NSKeyedUnarchiver.unarchiveObject(with: data) as! [Int:Int]
        guard let output = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) else { return [:] }
        return output as! [Int:Int]
    }
    func setListService(value: [String]) {
        setValue(value, forKey: UserDefaultKeys.listofservice.id)
    }
    
    func getListService() -> [String] {
        return object(forKey: UserDefaultKeys.listofservice.id) as! [String]
    }
    
    //MARK: - Total
    func setTotal(value: Double) {
        setValue(value, forKey: UserDefaultKeys.total.id)
    }
    
    func getTotal() -> Double {
        return object(forKey: UserDefaultKeys.total.id) as! Double
    }
    
    //MARK: - Note
    func setNote(value: String? = "") {
        setValue(value, forKey: UserDefaultKeys.note.id)
    }
    func getNote() -> String? {
        return object(forKey: UserDefaultKeys.note.id) as? String
    }
    
    //MARK: - Booking ID
    func setBookingId(value: Int) {
        setValue(value, forKey: UserDefaultKeys.booking_id.id)
    }
    func getBookingId() -> Int {
        return object(forKey: UserDefaultKeys.booking_id.id) as! Int
    }
    
    //MARK: - First letter name
    func setFirstletter(value: String) {
        setValue(value, forKey: UserDefaultKeys.f_letter.id)
    }
    func getFirstletter() -> String {
        return object(forKey: UserDefaultKeys.f_letter.id) as! String
    }
}
