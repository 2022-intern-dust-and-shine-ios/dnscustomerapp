//
//  Profile1ViewController.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 1/18/22.
//

import UIKit
import Alamofire

enum ManageProfileEnum {
    
    case info
    case account
    case card
    
    var storyboard: UIStoryboard {
        switch self {
        case .info, .account, .card:
            return UIStoryboard(name: "Profile", bundle: nil)
        }
    }
    var identifier: String {
        switch self {
        case .info:
            return "PersonalInformationViewController"
        case .account, .card:
            return ""
        }
    }
    var instantiateWithIdentifier: UIViewController? {
        switch self {
        case .info:
            return storyboard.instantiateViewController(withIdentifier: identifier)
        case .account, .card:
            return UIViewController()
        }
    }
    
}

class ProfileViewController: UIViewController{

    @IBOutlet weak var profileTableView: UITableView!
    
    enum Constant {
        static let identifier: String = "ProfileTableViewCell"
      
    }
    
    private let authService = AuthService()
    private let userDefaults = UserDefaults.standard
    private var loader : IVActivityIndicator?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.setTitle("Profile")
        self.navigationController?.navigationBar.setSyle(.white)
        self.navigationItem.backButtonTitle = ""
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loader = IVActivityIndicator(superView: self)
        configureTableView()
    }
    
    private func configureTableView() {
        profileTableView.delegate = self
        profileTableView.dataSource = self
        
        profileTableView.tableFooterView = UIView()
        profileTableView.tableHeaderView = UIView()
    }
    
    private func redirectToSignIn() {
        guard let viewController = Modules.SignIn.initialViewController as?
                MyNavigationController else { return }
        viewController.modalPresentationStyle = .fullScreen
        self.present(viewController, animated: true, completion: nil)
    }
    
    private func reset() {
        DispatchQueue.main.async {
            self.userDefaults.setToken(value: nil)
            //self.navigationController?.viewControllers.removeAll()
        }
    }
  
}

extension ProfileViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        return Profiles.allCases.count
    }
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let profile = Profiles.allCases[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: Constant.identifier,
                                                 for: indexPath)
        cell.textLabel?.text = profile.rawValue
        cell.textLabel?.font = UIFont(name: "Montserrat-Medium", size: 14)
        
        if indexPath.row == Profiles.allCases.count - 1 {
            cell.textLabel?.textColor = .red
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let row = indexPath.row
        if row == 0 {
            presentViewController(.info)
        } else if row == 1 {
            self.showAlert(message: "Unavailable right now. It's under maintenance")
        } else if row == 2 {
            self.showAlert(message: "Unavailable right now. It's under maintenance")
        } else {
            self.showAlertWithCompletion(title: "Log out",
                                         message: "Are you sure do you want to log out?",
                                         type: .logout) {
                self.logout()
            }
        }
    }
    
    func presentViewController(_ manageProfile: ManageProfileEnum) {
        guard let viewController = manageProfile.instantiateWithIdentifier as?
                PersonalInformationViewController else { return }
        self.navigationController?.pushViewController(viewController, animated: true)
    }
 
    func logout() {
        authService.logout(token: userDefaults.getToken() ?? "") { output, statusCode, error in
            guard let _ = output, error == nil else {
                print("ERROR \(error!.localizedDescription)")
                self.showAlert(message: "Please check your internet connection")
                return
            }
            //self.loader?.stopLoader()
            self.reset()
            self.redirectToSignIn()
        }
    }
}
