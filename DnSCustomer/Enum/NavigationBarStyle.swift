//
//  NavigationBarStyle.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 1/14/22.
//

import Foundation
import UIKit


public enum IVNavigatioNBarStyle {
    case white
    case transparent
    case hidden
    
    var barColor: UIColor {
        switch self {
        case .white:
            return .white
        case .transparent:
            return .clear
        case .hidden:
            return .clear
        }
    }
    var tintColor: UIColor {
        switch self {
        case .white:
            return .black
        case .transparent:
            return .white
        case .hidden:
            return .black
        }
    }
    var isHidden: Bool {
        switch self {
        case .white, .transparent:
            return false
        case  .hidden:
            return true
        }
    }
}
