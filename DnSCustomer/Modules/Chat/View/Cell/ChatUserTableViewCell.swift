//
//  ChatUserTableViewCell.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 1/17/22.
//

import UIKit

class ChatUserTableViewCell: UITableViewCell {
    
    static let identifier: String = "ChatUserTableViewCell"
   
    @IBOutlet weak var userImageView: UIImageView! {
        didSet {
            let radius = userImageView.frame.size.width / 2
            userImageView.setCornerRadius(radius)
            userImageView.clipsToBounds = true
        }
    }
    @IBOutlet weak var companynameLabel: UILabel!
    @IBOutlet weak var initialMessaegLabel: UILabel!
    @IBOutlet weak var datetimeLabel: UILabel!
    
    private var statusView: UIView = {
        let view = UIView()
        view.backgroundColor = .green
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setCornerRadius(6)
        return view
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
        
        contentView.addSubview(statusView)
        NSLayoutConstraint.activate([
            statusView.widthAnchor.constraint(equalToConstant: 12),
            statusView.heightAnchor.constraint(equalToConstant: 12),
            statusView.trailingAnchor.constraint(equalTo: userImageView.trailingAnchor),
            statusView.bottomAnchor.constraint(equalTo: userImageView.bottomAnchor)
        ])
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
