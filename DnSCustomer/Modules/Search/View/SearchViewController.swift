//
//  SearchViewController.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 1/20/22.
//

import UIKit

enum SearchType {
    case filter
    case all
}

class SearchViewController: UIViewController {
    
    //MARK: - IBOUTLETS
    
    @IBOutlet weak var searchTableView: UITableView!
    @IBOutlet weak var searchContainerView: UIView!
    @IBOutlet weak var searchTextField: UITextField! {
        didSet {
            searchTextField.addTarget(self, action: #selector(self.tfDidChange(_:)), for: .editingChanged)
        }
    }
    
    //MARK: - PRESENTER
    private lazy var presenter = CompanyPresenter(delegate: self) // mutable
    
    private let userDefault = UserDefaults.standard // immutable
    
    private var companies: Companies = [] {
        didSet {
            searchTableView.reloadData()
            
            if companies.count == 0 {
                let lbl = UILabel()
                lbl.numberOfLines = 2
                lbl.textColor = .gray
                lbl.text = "No company found"
                lbl.font = UIFont(name: "Montserrat-Medium", size: 16.0)
                lbl.frame = CGRect(origin: .zero, size: .init(width: 200, height: 60))
                lbl.translatesAutoresizingMaskIntoConstraints = false
                searchTableView.backgroundView = lbl
                lbl.centerXAnchor.constraint(equalTo: searchTableView.centerXAnchor).isActive = true
                lbl.centerYAnchor.constraint(equalTo: searchTableView.centerYAnchor).isActive = true
            } else {
                searchTableView.backgroundView = nil
            }
        }
    }
    var searchType: SearchType?
    
    //MARK: - APP LIFECYCLE
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.backButtonTitle = ""
        self.navigationController?.navigationBar.setSyle(.transparent)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if searchType == .filter {
            searchTextField.becomeFirstResponder()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchTextField.delegate = self
        
        initViews()
        configureTableView()
        fetchCompany("")
        
    }
    
    //MARK: - FUNCTIONS
    private func configureTableView() {
        searchTableView.delegate = self
        searchTableView.dataSource = self
        
        searchTableView.separatorStyle = .none
        searchTableView.tableFooterView = UIView()
    }
    private func initViews() {
        searchContainerView.layer.borderWidth = 0.5
        searchContainerView.layer.borderColor = AppColor.blueColor.cgColor
        searchContainerView.setCornerRadius(4)
        
      
        
    }
    
    private func redirectToDetailCompany () {
        guard let viewController = Modules.Detail.initialViewController as? CompanyDetailViewController else { return }
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    private func fetchCompany(_ keyword: String) {
        switch searchType {
        case .all:
            presenter.getListOfCompanies(userDefault.getToken()!)
        case .filter:
            if keyword.count != 0 {
                presenter.getSearchCompanies(keyword, userDefault.getToken()!)
            } else {
                companies = []
            }
        default:
            break
        }
    }
    //MARK: - OBJC ACTIONS
    @objc func tfDidChange(_ textfield: UITextField) {
        let keyword = textfield.text!
        fetchCompany(keyword)
    }
}

// MARK: - EXTENSIONS

extension SearchViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return companies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: SearchTableViewCell.identifier, for: indexPath) as? SearchTableViewCell {
            cell.bind(companies[indexPath.row])
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        userDefault.setCompanyRating(value: 0.0)
        userDefault.setCompanyId(value: companies[indexPath.row].id)
        searchTextField.resignFirstResponder()
        redirectToDetailCompany()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(199)
    }
}

extension SearchViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.searchType = .filter
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let keyword = textField.text!
        fetchCompany(keyword)
        textField.endEditing(true)
        return true
    }
}

extension SearchViewController: CompanyPresenterDelegate {
    func showFilteredCompanies(company: Company<Companies>) {
        print("FILTERED COMPANIES \(company)")
        companies = company.data
    }
    func showCompanies(company: Company<Companies>) {
        companies = company.data
    }
    func showError(_ error: String) {
        
    }
}

