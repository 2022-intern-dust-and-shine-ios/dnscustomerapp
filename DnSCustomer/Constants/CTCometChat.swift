//
//  CometChat.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 3/22/22.
//

import Foundation


enum CTCometChat {
    static let appId: String = "2055343da9e0c70c"
    static let authKey: String = "71cc1e22e3a4a36fb5a49ec0be00b980a714782e"
    static let region: String = "us"
}
