//
//  ForgotPassword.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 3/31/22.
//

import Foundation

struct InputForgotPassword: Codable {
    var email: String
}

struct OutputForgotPassword: Codable {
    var status: String?
    var message: String?
}



