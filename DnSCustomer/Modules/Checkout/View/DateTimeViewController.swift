//
//  DateTimeViewController.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 2/16/22.
//

import UIKit

class DateTimeViewController: UIViewController {

    @IBOutlet weak var datetimePicker: UIDatePicker!
    @IBOutlet weak var nextButton: UIButton! {
        didSet {
            nextButton.isEnabled = false
            nextButton.backgroundColor = .gray
        }
    }
    private let userDefaults = UserDefaults.standard
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.backButtonTitle = ""
        navigationItem.setTitle("Date & Time")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        datetimePicker.datePickerMode = .dateAndTime
        datetimePicker.addTarget(self,
                                 action: #selector(didChangeDatePicker(picker:)),
                                 for: .valueChanged)
        if #available(iOS 14.0, *) {
            datetimePicker.preferredDatePickerStyle = .inline
        } else {
            // Fallback on earlier versions
        }
   
    }
    
    @IBAction func didChangeDatePicker(picker: UIDatePicker) {
        let date = Date()
        let current_day = Calendar.current.component(.day, from: date)
        let current_month = Calendar.current.component(.month, from: date)
        let current_year = Calendar.current.component(.year, from: date)
        
        let selected_day = Calendar.current.component(.day, from: datetimePicker.date)
        let selected_month = Calendar.current.component(.month, from: datetimePicker.date)
        let selected_year = Calendar.current.component(.year, from: datetimePicker.date)
        
        if selected_year >= current_year {
            if selected_month == current_month {
                if selected_day > current_day {
                    nextButton.isEnabled = true
                    nextButton.backgroundColor = AppColor.yellowColor
                    return
                }
            } else if selected_month > current_month {
                nextButton.isEnabled = true
                nextButton.backgroundColor = AppColor.yellowColor
                return
            }
        }
        nextButton.isEnabled = false
        nextButton.backgroundColor = .gray
    }
    
    @IBAction func showDate(_ sender: UIButton) {
        readDateTimeString("yyyy-MM-dd hh:mm:ss")
        redirectToCheckout()
    }
    private func redirectToCheckout() {
        guard let viewController = Modules.Checkout.initialViewController as? CheckoutViewController
        else {
            return
        }
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

extension DateTimeViewController {
    func readDateTimeString(_ format: String) {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        let datetime = formatter.string(from: datetimePicker.date)
        userDefaults.setDateTime(value: datetime)
        
  
        
        
        print("SELECTED DATA \(datetime)")
    }
}
