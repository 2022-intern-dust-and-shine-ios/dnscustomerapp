//
//  BDModalViewController.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 3/24/22.
//

import UIKit

enum BookingModuleType {
    case booking_service
    case booking_history
}

class BDModalViewController: UIViewController {

    @IBOutlet weak var workerStackView: UIStackView!
    @IBOutlet weak var commentStackView: UIStackView!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var companynameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var telnumberLabel: UILabel!
    @IBOutlet weak var paymentstatusLabel: UILabel!
    @IBOutlet weak var datetimeLabel: UILabel!
    @IBOutlet weak var serviceLabel: UILabel!
    @IBOutlet weak var noteLabl: UILabel!
    @IBOutlet weak var workernameLabel: UILabel!
    @IBOutlet weak var mobilenumberLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var containerView: UIView! {
        didSet {
            containerView.setCornerRadius(8.0)
        }
    }
    @IBOutlet weak var closeButton: UIButton! {
        didSet {
            closeButton.setCornerRadius(8.0)
        }
    }
    
    //MARK: - PRESENTER
    private lazy var company_presenter = CompanyPresenter(delegate: self)
    
    private let userDefault = UserDefaults.standard
    
    var dataService: CData?
    var dataHistory: HData?
    var type: BookingModuleType = .booking_service
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        switch type {
        case .booking_service:
            displayBookingServiceDetails()
            commentStackView.isHidden = true
            workerStackView.isHidden = true
        case .booking_history:
            commentStackView.isHidden = false
            workerStackView.isHidden = true
            displayBookingHistoryDetails()
        }
        
       
    }
    
    private func displayCompanyData(company: MCompany) {
        companynameLabel.text = company.name
        emailLabel.text = company.email
        telnumberLabel.text = company.telNumber ?? ""
    }
    
    private func displayBookingServiceDetails() {
        guard let data = dataService else {
            return
        }
        

        company_presenter.getCompany(userDefault.getToken()!, data.companyId)
        
        paymentstatusLabel.text = data.paymentStatus == 0 ? "Not yet Paid" : "Payment Acknowledged"
        datetimeLabel.text = data.schedDateTime
        serviceLabel.text = book_services(data.services)
        noteLabl.text = data.note ?? ""
        totalLabel.text = "₱\(data.total)"
        
        guard let worker = data.workers.first else {
            workernameLabel.text = "N/A"
            mobilenumberLabel.text = "N/A"
            return }
            let fullname = "\(worker.firstName ?? "") \(worker.lastName ?? "")"
            
            workernameLabel.text = fullname
            mobilenumberLabel.text = worker.mobileNumber
        
    }
    
    private func displayBookingHistoryDetails() {
        guard let data = dataHistory else {
            return
        }
        
        company_presenter.getCompany(userDefault.getToken()!, data.companyId)
        
        paymentstatusLabel.text = data.paymentStatus == 0 ? "Not yet Paid" : "Payment Acknowledged"
        datetimeLabel.text = data.schedDateTime
        serviceLabel.text = book_history(data.services)
        noteLabl.text = data.note ?? ""
        totalLabel.text = "₱\(data.total)"
        
        if let reviews = data.reviews, let firstReview = reviews.first {
            commentLabel.text = firstReview.comment
        }
        
    }
    
    private func book_services(_ services: WServices) -> String {
        var service_str = ""
        for service in services {
            service_str+="\(service.name), "
        }
        if service_str.count > 2 {
            service_str.removeLast(2)
        }
        return service_str
    }
    
    private func book_history(_ services: HServices) -> String {
        var service_str = ""
        for service in services {
            service_str+="\(service.name), "
        }
        if service_str.count > 2 {
            service_str.removeLast(2)
        }
     
        return service_str
    }
    
    @IBAction func closeAction( _ sender: UIButton){
        dismiss(animated: false, completion: nil)
    }
}

extension BDModalViewController: CompanyPresenterDelegate {
    func showError(_ error: String) {
        
    }
    
    func showCompany(_ company: Company<MCompany>) {
        displayCompanyData(company: company.data)
    }
}

