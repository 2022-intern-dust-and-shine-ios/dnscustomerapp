//
//  FeaturedServicePresenter.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 1/24/22.
//

import Foundation

class FService : NSObject {
    
    var services: ((FeaturedServices) -> Void)?
    
    func getFeaturedService() {
        
        let data = Bundle.main.loadJsonResource(path: "featuredservice")
        decode(localData: data)
    }
    private func decode(localData: Data) {
        DispatchQueue.main.async {
            do {
                let result = try JSONDecoder().decode(FeaturedServices.self, from: localData)
                self.services?(result)
        
            } catch let err as NSError {
                print(err.localizedDescription)
            }
        }
    }
}

protocol FeaturedServicePresenterDelegate: AnyObject {
    func showFeaturedServices(services: MCompanyService)
    func showError(statusCode: Int)

}

class FeaturedServicePresenter {
    
    private weak var delegate: FeaturedServicePresenterDelegate?
    private let companyService = CompanyService()
    
    init(delegate: FeaturedServicePresenterDelegate) {
        self.delegate = delegate
    }
    
    func getFeaturedService(_ token: String) {
        companyService.getServices(token: token) { [weak self] services, statusCode, error in
            guard let self = self else { return }
            guard let services = services, error == nil else {
                self.delegate?.showError(statusCode: statusCode ?? 0)
                return }
            self.delegate?.showFeaturedServices(services: services)
        }
    }
  
}
