//
//  Service.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 2/9/22.
//

import Foundation


struct MCompanyService: Codable {
    
    var data: MServices
    var message: String
}

struct MService: Codable {
    var id: Int
    var name: String
    var description: String
    var createdAt: String
    var updatedAt: String
    var pivot: MPivot?
    var time: String
    var price: Double
    var serviceImage: String
    
    enum CodingKeys: String, CodingKey {
        case id, name, description, pivot, time, price
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case serviceImage = "service_image"
    }
}
typealias MServices = [MService]

struct MPivot: Codable {
    var companyId: Int
    var serviceId: Int
    enum CodingKeys: String, CodingKey {
        case companyId = "company_id"
        case serviceId = "service_id"
    }
}
