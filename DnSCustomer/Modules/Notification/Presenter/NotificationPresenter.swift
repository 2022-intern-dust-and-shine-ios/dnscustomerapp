//
//  NotificationPresenter.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 3/9/22.
//

import Foundation



protocol NotificationPresenterDelegate: AnyObject {
    func showNotifications(notifications: OutputNotification)
    func showError(statusCode: Int)
}

class NotificationPresenter: NSObject {
    
    private let customerService = CustomerService()
    private weak var delegate: NotificationPresenterDelegate?
    
    init(delegate: NotificationPresenterDelegate) {
        self.delegate = delegate
    }
    
    public func getNotifications(token: String) {
        customerService.getNotifications(token: token) {
            [weak self] notification, statusCode, error in
            guard let self = self else { return }
            guard let notification = notification, error == nil else {
                print(String(describing: error))
                self.delegate?.showError(statusCode: statusCode ?? 0)
                return }
            self.delegate?.showNotifications(notifications: notification)
        }
    }
}
