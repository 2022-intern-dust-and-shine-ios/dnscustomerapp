//
//  UIView+Shadow.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 1/14/22.
//

import Foundation
import UIKit


extension UIView {
    func addShadow() {
        self.layer.cornerRadius = 8
        self.layer.shadowColor = AppColor.shadowColor.cgColor
        self.layer.shadowOffset = CGSize(width: CGFloat(0.0), height: CGFloat(0.0))
        self.layer.shadowRadius = 4
        self.layer.shadowOpacity = 0.4
    }
    func lightShadow() {
       
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: CGFloat(0.0), height: CGFloat(0.0))
        self.layer.shadowRadius = 4
        self.layer.shadowOpacity = 0.2
    }
    func addShadowNoCorner() {
 
        self.layer.shadowColor = AppColor.shadowColor.cgColor
        self.layer.shadowOffset = CGSize(width: CGFloat(0.0), height: CGFloat(0.0))
        self.layer.shadowRadius = 4
        self.layer.shadowOpacity = 0.4
    }
}
