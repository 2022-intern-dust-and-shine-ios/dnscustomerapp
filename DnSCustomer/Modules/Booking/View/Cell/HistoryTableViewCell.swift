//
//  HistoryTableViewCell.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 3/9/22.
//

import UIKit

class HistoryTableViewCell: UITableViewCell {

    static let identifier: String = "HistoryTableViewCell"
    @IBOutlet weak var nameContainerView: UIView! {
        didSet {
            let radius = nameContainerView.frame.size.width / 2
            nameContainerView.setCornerRadius(radius)
            nameContainerView.clipsToBounds = true
        }
    }
    @IBOutlet weak var servicesLabel: UILabel!
    @IBOutlet weak var firstletterLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var commentLabel: UILabel!

    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var paymentLabel: UILabel!
    @IBOutlet weak var datetimeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        selectionStyle = .none
        containerView.lightShadow()
        containerView.setCornerRadius(6.0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func services(services: HServices) -> String {
        var servicesText = ""
        for service in services {
            servicesText+="\(service.name), "
        }
        if servicesText.count > 2 {
            servicesText.removeLast(2)
        }
        
        return servicesText
    }
    
    public func bind(_ data: HData, letter: String) {
    
        if let review = data.reviews?.first {
            commentLabel.text = review.comment
        }
        servicesLabel.text = services(services: data.services)
        datetimeLabel.text = data.schedDateTime
        totalLabel.text = "₱\(data.total)"
        firstletterLabel.text = letter
        paymentLabel.text = data.paymentStatus == 1 ? "Payment Acknowledged" : "Not yet paid"
    }
}
