//
//  CustomerAPI.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 2/11/22.
//

import Foundation
import Alamofire


enum CustomerAPI {
    case user(_ token: String)
    
    case update(_ id: Int, _ token: String, _ input: InputUser)
    case change_password(_ token: String, _ input: InputChangePassword)
    case forgot_password(_ input: InputForgotPassword)
    case reset_password(_ input: InputResetPassword)
    
    case checkout(_ input: InputCheckout, _ token: String)
    case bookingservice(_ token: String)
    case history(_ token: String)
    case reviews(_ token: String, _ input: InputReviews)
    case notification(_ token: String)
}

extension CustomerAPI: URLRequestConvertible, EndPointType {
    func asURLRequest() throws -> URLRequest {
        let url = baseURL.appendingPathComponent(path)
        var request = URLRequest(url: url)
        request.method = method
        request.headers = headers
        
        switch self {
        case .checkout(let input, _):
            request = try JSONParameterEncoder().encode(input, into: request)
        case .bookingservice(_), .history(_), .notification(_):
            request = try URLEncodedFormParameterEncoder().encode(["":""], into: request)
        case .reviews(_, let input):
            request = try JSONParameterEncoder().encode(input, into: request)
        case .user(_):
            request = try URLEncodedFormParameterEncoder().encode(["":""], into: request)
        case .update(_, _, let input):
            request = try JSONParameterEncoder().encode(input, into: request)
        case .change_password(_, let input):
            request = try JSONParameterEncoder().encode(input, into: request)
        case .forgot_password(let input):
            request = try JSONParameterEncoder().encode(input, into: request)
        case .reset_password(let input):
            request = try JSONParameterEncoder().encode(input, into: request)
        }
        return request
    }
    
    var baseURL: URL {
        return URL(string: "https://lpn.boomtech.co/api")!
    }
    
    var path: String {
        switch self {
        case .checkout, .bookingservice:
            return "/bookings"
        case .history:
            return "/history"
        case .reviews:
            return "/reviews"
        case .notification:
            return "/notification"
        case .user:
            return "/user-management"
        case .update(let id, _, _):
            return "/user-management/\(id)"
        case .change_password:
            return "/change-password"
        case .forgot_password:
            return "/forgot-password"
        case .reset_password:
            return "/reset-password"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .checkout, .reviews:
            return .post
        case .change_password, .forgot_password, .reset_password:
            return .post
        case .bookingservice, .history, .notification:
            return .get
        case .user:
            return .get
        case .update:
            return .put
        }
    }
    
    var headers: HTTPHeaders {
        switch self {
        case .checkout(_, let token), .bookingservice(let token),
                .history(let token), .reviews(let token, _),
                .notification(let token), .user(let token),
                .update(_, let token, _), .change_password(let token, _):
            return [.accept("application/json"), .authorization(bearerToken: token)]
        case .forgot_password, .reset_password:
            return [.accept("application/json")]
        }
    }
}

