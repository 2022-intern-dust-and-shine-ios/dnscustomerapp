//
//  AuthService.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 1/31/22.
//

import Foundation
import Alamofire
import RealmSwift

typealias AuthServiceCompletionHandler<T:Codable> = ((T?, Int?, AFError?) -> Void)

struct AuthService {
    
    public init() {}

    public func register(input: InputRegister, completion: @escaping AuthServiceCompletionHandler<OutputRegister>) {
        AF.request(AuthAPI.register(input)).responseDecodable(of: OutputRegister.self)
        { response in
            let statusCode = response.response?.statusCode
            switch response.result {
            case .success(let output):
                completion(output, statusCode ,nil)
            case .failure(let error):
                print("REGISTER ERROR \(String(describing: error))")
                completion(nil, statusCode ,error)
            }
        }
        .cURLDescription { description in
            print(description)
        }
    }
    
    public func login(input: InputLogin, completion: @escaping AuthServiceCompletionHandler<OutputLogin>) {
        AF.request(AuthAPI.login(input)).responseDecodable(of: OutputLogin.self)
        { response in
            let statusCode = response.response?.statusCode
            switch response.result {
            case .success(let output):
                completion(output, statusCode ,nil)
            case .failure(let error):
                completion(nil,statusCode, error)
            }
        }
        .cURLDescription { description in
            print(description)
        }
    }
    
    public func logout(token: String, completion: @escaping AuthServiceCompletionHandler<OutputLogout>) {
        AF.request(AuthAPI.logout(token)).responseDecodable(of: OutputLogout.self)
        { response in
            let statusCode = response.response?.statusCode
            switch response.result {
            case .success(let output):
                completion(output, statusCode, nil)
            case .failure(let error):
                completion(nil, nil, error)
            }
        }
        .cURLDescription { description in
            print(description)
        }
    }
}

