//
//  NotificationViewController.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 1/20/22.
//

import UIKit

class NotificationViewController: UIViewController {

    @IBOutlet weak var notificationTableView: UITableView!
    
    private var refreshControl = UIRefreshControl()
    
    private var loaderIndicatorView : UIActivityIndicatorView = {
        let loader = UIActivityIndicatorView()
        loader.hidesWhenStopped = true
        loader.style = .medium
        return loader
    }()
    
    //MARK: - PRESENTER
    private lazy var presenter = NotificationPresenter(delegate: self)
    private let userDefault = UserDefaults.standard

    //MARK: - PROPERTIES
    private var notifications: [NData] = [] {
        didSet {
            notificationTableView.reloadData()
            
            if notifications.count != 0 {
                notificationTableView.backgroundView = nil
            } else {
                notificationTableView.empty(.notif)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.backButtonTitle = ""
        self.navigationItem.setTitle("Notification")
        navigationController?.navigationBar.setSyle(.transparent)
        presenter.getNotifications(token: userDefault.getToken()!)
     
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        startLoader()
        configureTableView()
        setupRefreshControl()
     
        
    }
    

    
    private func startLoader() {
        loaderIndicatorView.frame = CGRect(origin: .zero,
                                           size: .init(width: 34,
                                                       height: 34))
        loaderIndicatorView.startAnimating()
        notificationTableView.tableHeaderView = loaderIndicatorView
    }
    
    private func stopLoader() {
        self.loaderIndicatorView.stopAnimating()
        self.notificationTableView.tableHeaderView = nil
    }
    
    private func configureTableView() {
        notificationTableView.delegate = self
        notificationTableView.dataSource = self
    }
    
    private func setNotificationContent(services: HServices, workers: NWorkers) -> [String] {
        var notif_content = [String]()
        var services_str = ""
        var workers_str = ""
        for service in services {
            services_str+="\(service.name), "
        }
        for worker in workers {
            let name = "\(worker.firstName) \(worker.lastName)"
            workers_str+=name
        }
        if services_str.count > 2 && workers_str.count > 2 {
            services_str.removeLast(2)
            workers_str.removeLast(2)
        }

        notif_content.append(services_str)
        notif_content.append(workers_str)
        return notif_content
    }
  
    private func setupRefreshControl() {
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        notificationTableView.addSubview(refreshControl)
    }
    
    @objc func refresh(_ sender: AnyObject) {
        if Connectivity.isConnectedToInternet() {
            presenter.getNotifications(token: userDefault.getToken()!)
        } else {
            self.showAlertWithCompletion(actionTitle:"Okay",
                                         message: "Please check your internet connection") {
                self.refreshControl.endRefreshing()
            }
        }
     
    }
}

extension NotificationViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notifications.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotifCell", for: indexPath)
        
        let notification = notifications[indexPath.row]
        let contents = setNotificationContent(services: notification.services,
                                             workers: notification.workers)
        cell.textLabel?.text = "\(contents.first ?? "") service is done!"
        cell.detailTextLabel?.text = contents.last ?? ""
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        userDefault.setBookingId(value: notifications[indexPath.row].id)
        guard let viewController = storyboard?.instantiateViewController(withIdentifier: "FeedbackViewController") as? FeedbackViewController else { return }
        self.navigationController?.pushViewController(viewController, animated: true)
        
//        let notification = notifications.first
//        if let service = notification?.services[indexPath.row] {
//
//        }
    }
}

extension NotificationViewController: NotificationPresenterDelegate {
    func showError(statusCode: Int) {
        stopLoader()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.refreshControl.endRefreshing()
       
        }
    }
    
    func showNotifications(notifications: OutputNotification) {
        stopLoader()
        self.notifications = notifications.data
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.refreshControl.endRefreshing()
        }
    }
}
