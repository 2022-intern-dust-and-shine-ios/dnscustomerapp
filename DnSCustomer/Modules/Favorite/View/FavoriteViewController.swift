//
//  FavoriteViewController.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 1/17/22.
//

import UIKit

class FavoriteViewController: UIViewController {

    //MARK: - IBOUTLETS
    @IBOutlet weak var favoriteTableView: UITableView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.setTitle("Favorite Companies")
        self.navigationController?.navigationBar.setSyle(.white)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureTableView()
    }
    
    private func configureTableView() {
        favoriteTableView.delegate = self
        favoriteTableView.dataSource = self
        
        favoriteTableView.separatorStyle = .none
    }
}

extension FavoriteViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: FavoriteTableViewCell.identifier, for: indexPath) as? FavoriteTableViewCell {
            
            return cell
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(199)
    }
    
}
