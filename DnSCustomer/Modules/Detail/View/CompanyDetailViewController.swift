//
//  CompanyDetailViewController.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 1/20/22.
//


import UIKit
import SDWebImage

class CompanyDetailViewController: UIViewController {
    
    enum Constants {
        static let service_identifier = "CompanyServiceViewController"
        static let datetime_identifier = "DateTimeViewController"
    }
    
    //MARK: - OUTLETS
    @IBOutlet weak var companyImageView: UIImageView!

    @IBOutlet weak var serviceStackView: UIStackView!
    @IBOutlet weak var serviceLabelStackView: UIStackView! {
        didSet {
            serviceLabelStackView.isHidden = false
        }
    }
    @IBOutlet weak var constraintLayout: NSLayoutConstraint!
    
    @IBOutlet weak var nameContainerView: UIView! {
        didSet {
            nameContainerView.setCornerRadius(8.0)
        }
    }
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var detailContainerView: UIView!
    @IBOutlet weak var ratingContainerView: UIView!
    @IBOutlet weak var noteTextView: UITextView! {
        didSet {
            noteTextView.text = ""
        }
    }
    
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var companynameLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var companynamelabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var mobilenumberLabel: UILabel!
    @IBOutlet weak var telphonenumberLabel: UILabel!
    @IBOutlet weak var noavailableStackView: UIStackView! {
        didSet {
            noavailableStackView.isHidden = true
        }
    }
    
    private lazy var messageBarButton: UIBarButtonItem = {
        let messageButton = UIBarButtonItem(image: UIImage(named: "message")!,
                                            style: .plain, target: self,
                                            action: #selector(messageAction(sender:)))
        messageButton.tintColor = .white
        messageButton.customView?.addShadowNoCorner()
        return messageButton
    }()
    private lazy var favoriteBarButton: UIBarButtonItem = {
        let favoriteButton = UIBarButtonItem(image: UIImage(named: "heart-red")!,
                                             style: .plain, target: self,
                                             action: #selector(favoriteAction(sender:)))
        favoriteButton.tintColor = .red
        favoriteButton.customView?.addShadow()
        return favoriteButton
    }()

    //MARK: - Properties
    private var subControllers: [CompanyServiceViewController] = []
    private var servicesname: [String] = []
    private var services: [Int:Int] = [:]
    private var tempoServices: [Int:Int] = [:]
    private var total: Double = 0
    private var key: Int = 0
    
    //MARK: - PRESENTER
    private lazy var presenter = CompanyPresenter(delegate: self)
    
    //MARK: - SERVICE
    private lazy var keyboardService = KeyboardService(delegate: self)
    private lazy var toolbaService = ToolbarService(delegate: self)
    
    private let userDefault = UserDefaults.standard
    var loader: IVActivityIndicator?

    var heightConstant: CGFloat = 50
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.setSyle(.transparent)
        self.navigationItem.backButtonTitle = ""
        
        //self.navigationItem.rightBarButtonItems = [favoriteBarButton, messageBarButton]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loader = IVActivityIndicator(superView: self)
        loader?.showLoader(navController: navigationController ?? self)
        
        initViews()
        getServicesAndCompanies()
    
        
        keyboardService.setupRegisterKbNotification()
        toolbaService.setupToolbar(textviews: [noteTextView], type: .textview)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        constraintLayout.constant = CGFloat(1.0)
        super.touchesBegan(touches, with: event)
    }
    
    private func initViews() {
       
        [companynameLabel, locationLabel, ratingLabel].forEach { $0?.dropShadow() }
        detailContainerView.addShadow()
        containerView.layer.borderWidth = 1.0
        containerView.layer.borderColor = AppColor.shadowColor.cgColor
        containerView.setCornerRadius(8.0)
        serviceStackView.layoutMargins = .init(top: 10, left: 12, bottom: 12, right: 12)
        serviceStackView.isLayoutMarginsRelativeArrangement = true
        ratingContainerView.setCornerRadius(8.0)
    }
    
    private func getServicesAndCompanies() {
        let token = userDefault.getToken()!
        //presenter.getCompanyServices(token)
        presenter.getListOfCompanies(token)
        presenter.getCompany(token, userDefault.getCompanyId())
    }
    
    
    private func addChildeControllers(_ mservices: MServices) {
   
        for (index,service) in mservices.enumerated() {
            
            guard let vc = storyboard?.instantiateViewController(withIdentifier: Constants.service_identifier)
                    as? CompanyServiceViewController else { return }
            
            vc.view.translatesAutoresizingMaskIntoConstraints = false
            vc.bind(service, index, key)
            
            serviceStackView.addArrangedSubview(vc.view)
            vc.didMove(toParent: self)
            subControllers.append(vc)
            
            tempoServices[key] = 0
            key+=1
            
            vc.plusTotal = { [weak self] (price) in
                guard let self = self else { return }
                self.total+=price
                print(price)
                print("TOTAL \(self.total)")
            }
            vc.minusTotal = { [weak self] (price) in
                guard let self = self else { return }
                self.total-=price
                print(price)
                print("TOTAL \(self.total)")
            }
            
            vc.selectedService = { [weak self] (service,flag,index, key, total) in
                guard let self = self else  { return }
                if flag {
                    if let index = self.servicesname.firstIndex(of: service.name) {
                        self.servicesname[index] = service.name
                    }
                    self.servicesname.append(service.name)
                    self.tempoServices[key] = service.id
                    //self.total+=service.price
               
                } else  {
                    
                    if let index = self.servicesname.firstIndex(of: service.name) {
                        self.servicesname[index] = ""
                    }
                    if !self.tempoServices.isEmpty {
                        self.total-=total
              
                        if self.total < 0 {
                            self.total = 0
                        }
                        //self.total = 0
                        self.tempoServices[key] = 0
                      
                    }
                }
            }
        }
    }
    
    private func validateService() -> Bool {
        key = 0
        for (_, val) in tempoServices.enumerated() {
            if val.value != 0 {
                services[key] = val.value
                key+=1
            }
        }
        
        if let index = servicesname.firstIndex(where: { $0 == ""}) {
            servicesname.remove(at: index)
        }
        
        if services.count != 0 {
            return true
        } else {
            return false
        }
    }

    //MARK: - ACTIONS
    @objc func didTapDone() {
        view.endEditing(true)
        constraintLayout.constant = CGFloat(1.0)
    }
    
    @objc func favoriteAction(sender: UIBarButtonItem) {
        print("SERVICE NAME \(servicesname)")
        print("TOTAL \(total)")
    }
    
    @objc func messageAction(sender: UIBarButtonItem) {
        guard let viewController = Modules.Chat.initialViewController as?
                UINavigationController else {
                    print("ERROR NAVIGFATYION")
                    return }
        self.present(viewController, animated: true, completion: nil)
    }
  
    @IBAction func nextAction(_ sender: UIButton) {
        let notes = noteTextView.text
        print(total)
        if validateService() {
            userDefault.setListService(value: servicesname)
            userDefault.setServices(value: services)
            userDefault.setTotal(value: total)
            userDefault.setNote(value: notes ?? "")
            guard let viewController = Modules.Checkout.storyboard.instantiateViewController(withIdentifier: Constants.datetime_identifier)
                    as? DateTimeViewController
                    else { return }
            self.navigationController?.pushViewController(viewController, animated: true)
        } else {
            self.showAlert(actionTitle: "Okay", message: "No Service selected")
        }
    }
}

extension CompanyDetailViewController: CompanyPresenterDelegate {
    func showCompany(_ company: Company<MCompany>) {
        loader?.stopLoader()
        
        userDefault.setCompanyImage(value: company.data.companyImage)
        ratingLabel.text = "\(userDefault.getCompanyRating() ?? 0.0)"
        
        companynamelabel.text = company.data.name
        addressLabel.text = company.data.address
        emailLabel.text = "Email: \(company.data.email)"
        mobilenumberLabel.text = "Mobile number: \(company.data.mobileNumber)"
        telphonenumberLabel.text = "Telephone number: \(company.data.telNumber ?? "")"
        let urlImage = URL(string: "https://lpn.boomtech.co/\(company.data.companyImage)")
        companyImageView.sd_setImage(with: urlImage,
                                     placeholderImage: UIImage(),
                                     options: .refreshCached)
        companyImageView.sd_imageTransition = .fade(duration: 2.0)
        
        if let services = company.data.services {
            if services.count != 0 {
                addChildeControllers(services)
                noavailableStackView.isHidden = true
                serviceLabelStackView.isHidden = false
            } else {
                serviceLabelStackView.isHidden = true
                noavailableStackView.isHidden = false
            }
        }
        
    }
    
    func showError(_ error: String) {
        loader?.stopLoader()
        self.showAlert(message: "Unable to process your request right now, Please try again later")
        print("ERROR \(error)")
    }
}

extension CompanyDetailViewController: KeyboardProtocol {
    func getKeyboardSize(size: CGSize) {
        constraintLayout.constant = size.height
    }
}

extension CompanyDetailViewController: ToolbarProtocol {
    func dismissKeyboard() {
        self.view.endEditing(true)
        constraintLayout.constant = 1.0
    }
}

class GradientView: UIView {

    override func awakeFromNib() {
        super.awakeFromNib()

        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = CGRect(x: 0, y: 0, width: frame.width, height: frame.height)
    
//        let startColor = UIColor(red: 30/255, green: 113/255, blue: 79/255, alpha: 0).cgColor
//        let endColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1).cgColor
//        gradient.colors = [startColor, endColor]
//        gradientLayer.startPoint = CGPoint(x: 0.0, y: 1.0)
//        gradientLayer.endPoint = CGPoint(x: 1.0, y: 1.0)
        let startColor = UIColor(red: 30/255, green: 113/255, blue: 79/255, alpha: 0).cgColor
        
        let endColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1).cgColor
        gradientLayer.colors = [endColor, startColor]
        layer.addSublayer(gradientLayer)
    }
}
