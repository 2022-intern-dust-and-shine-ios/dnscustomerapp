//
//  OutputLogout.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 1/31/22.
//

import Foundation



struct OutputLogout: Codable {
    var message: String
}
