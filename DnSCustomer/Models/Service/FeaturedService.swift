//
//  FeaturedService.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 1/24/22.
//

import Foundation
import UIKit




struct FeaturedService: Codable {
    
    var id: Int
    var title: String
    var subtitle: String
    var image: String
    
}

typealias FeaturedServices = [FeaturedService]
