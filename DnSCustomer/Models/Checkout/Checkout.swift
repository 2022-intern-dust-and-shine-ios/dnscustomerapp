//
//  Booking.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 2/11/22.
//

import Foundation

struct InputCheckout: Codable {
    var companyId: Int
    var address: String
    var schedDatetime: String
    var total: Double
    var services: Dictionary<Int, Int>
    var note: String?
    
    enum CodingKeys: String, CodingKey {
        case address, total, services, note
        case companyId = "company_id"
        case schedDatetime = "sched_datetime"
    }
}

struct OutputCheckout: Codable {
    var data: WData
    var message: String
}

struct WData: Codable {
    var userId: Int
    var status: Int
    var companyId: Int
    var address: String
    var total: Int
    var updatedAt: String
    var createdAt: String
    var id: Int
    var services: WServices
    var workers: [Worker]
    var customer: User

    enum CodingKeys: String, CodingKey {
        case id, status, address, total, services,workers, customer
        case userId = "user_id"
        case companyId = "company_id"
        case updatedAt = "updated_at"
        case createdAt = "created_at"
    }
}

public typealias WServices = [WService]

public struct WService: Codable {
    var id: Int
    var name: String
    var description: String
    var createdAt: String
    var updatedAt: String
    var pivot: Pivot
    enum CodingKeys: String, CodingKey {
        case id, name, description, pivot
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
}

struct Pivot: Codable {
    var bookingId: Int
    var serviceId: Int
    enum CodingKeys: String, CodingKey {
        case bookingId = "booking_id"
        case serviceId = "service_id"
    }
}

public typealias Workers = [Worker]

public struct Worker: Codable {
    var id: Int?
    var firstName: String?
    var lastName: String?
    var mobileNumber:String?
    var email: String?
    var emailVerifiedAt: String?
    var createdAt: String?
    var updatedAt: String?
    var pivot: NPivot?
}




