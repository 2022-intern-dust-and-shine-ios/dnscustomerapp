//
//  SearchTableViewCell.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 1/20/22.
//

import UIKit

class SearchTableViewCell: UITableViewCell {

    static let identifier: String = "SearchTableViewCell"
    @IBOutlet weak var companyImage: UIImageView! {
        didSet {
            companyImage.setCornerRadius(8)
            companyImage.clipsToBounds = true
            companyImage.lightShadow()
        }
    }
    @IBOutlet weak var companynameLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var containerView: UIView! {
        didSet {
            containerView.lightShadow()
            containerView.setCornerRadius(8)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        selectionStyle = .none
        
        companynameLabel.dropShadow()
        locationLabel.dropShadow()
   
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    public func bind(_ company: MCompany) {
        companynameLabel.text = company.name
        locationLabel.text = company.address
   
        let urlImage = URL(string: "https://lpn.boomtech.co/\(company.companyImage)")
        companyImage.sd_setImage(with: urlImage, placeholderImage: UIImage(), options: .refreshCached)
        companyImage.sd_imageTransition = .fade(duration: 2.0)
    }
}
