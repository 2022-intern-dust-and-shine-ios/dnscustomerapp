//
//  Company.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 2/10/22.
//

import Foundation


//TODO: - Refactor this to all related company models
/// Todo: - Generics model Company

struct Company<T:Codable> : Codable {
    var data: T
    var message: String
}

struct MCompany: Codable {
    var id: Int
    var name: String
    var email: String
    var mobileNumber: String
    var telNumber: String?
    var address: String
    var createdAt: String
    var updatedAt: String
    var companyImage: String
    var services: MServices?
    var pivot: MPivot?
    
    enum CodingKeys: String, CodingKey {
        case id, name, email, address, services, pivot
        case mobileNumber = "mobile_number"
        case telNumber = "tel_number"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case companyImage = "company_image"
    }
}

typealias Companies = [MCompany]


