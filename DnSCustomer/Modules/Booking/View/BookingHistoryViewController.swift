//
//  BookingHistoryViewController.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 1/19/22.
//

import UIKit

class BookingHistoryViewController: UIViewController {

    //MARK: - OUTLETS
    @IBOutlet weak var historyTableView: UITableView!
    
    private var refreshControl = UIRefreshControl()
    
    private var loaderIndicatorView : UIActivityIndicatorView = {
        let loader = UIActivityIndicatorView()
        loader.hidesWhenStopped = true
        loader.style = .medium
        return loader
    }()
    
    //MARK: - Presenter
    private lazy var presenter = BookingServicePresenter(delegate: self)
    
    //MARK: - Properties
    private var histories: [HData] = [] {
        didSet {
            historyTableView.reloadData()
            
            if histories.count == 0 {

                historyTableView.empty(.history)
            } else {
                historyTableView.backgroundView = nil
            }
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.setTitle("Booking History")
    }
    
    private let userDefault = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureTableView()
        startLoader()
        setupRefreshControl()
        presenter.getBookingHistory(token: userDefault.getToken()!)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
            
        refreshControl.endRefreshing()
        stopLoader()
    }
    
    private func configureTableView() {
        historyTableView.delegate = self
        historyTableView.dataSource = self
        historyTableView.separatorStyle = .none
    }
    
    private func startLoader() {
        loaderIndicatorView.frame = CGRect(origin: .zero,
                                           size: .init(width: 34, height: 34))
        loaderIndicatorView.startAnimating()
        historyTableView.tableHeaderView = loaderIndicatorView
    }
    private func stopLoader() {
        self.loaderIndicatorView.stopAnimating()
        self.historyTableView.tableHeaderView = nil
    }
    
    private func setupRefreshControl() {
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        historyTableView.addSubview(refreshControl)
    }
    
    private func redirectToModal(data: HData) {
        guard let viewController = storyboard?.instantiateViewController(withIdentifier: "BDModalViewController") as? BDModalViewController else { return }
        viewController.dataHistory = data
        viewController.type = .booking_history
        viewController.modalPresentationStyle = .overCurrentContext
        present(viewController, animated: false, completion: nil)
        
    }
    
    @objc func refresh(_ sender: AnyObject) {
        if Connectivity.isConnectedToInternet() {
            presenter.getBookingHistory(token: userDefault.getToken()!)
        } else {
            
            self.showAlertWithCompletion(actionTitle:"Okay",
                                         message: "Please check your internet connection") {
                self.refreshControl.endRefreshing()
            }
        }
        
    }
}

extension BookingHistoryViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return histories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: HistoryTableViewCell.identifier, for: indexPath) as? HistoryTableViewCell else { return UITableViewCell() }
        cell.bind(histories[indexPath.row], letter: userDefault.getFirstletter())
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(150.0)
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if ModalFeatureFlag.flag {
            redirectToModal(data: histories[indexPath.row])
        }
    }
}

extension BookingHistoryViewController: BookingServicePresenterDelegate {

    func showBookHistory(history: OutputHistory) {
        self.stopLoader()
        var histories: [HData] = []
        histories = history.data.sorted(by: { lhs, rhs in
            return lhs.id > rhs.id
        })
        self.histories = histories
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.refreshControl.endRefreshing()
        }
    }
    
    func showError(_ error: String) {
        stopLoader()
        print("ERROR \(error)")
    }
}

