//
//  FeaturedServiceCVC.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 1/19/22.
//

import UIKit
import SDWebImage

class FeaturedServiceCVC: UICollectionViewCell {
    
    static let identidier: String = "FeaturedServiceCVC"
    @IBOutlet weak var featureImage: UIImageView! {
        didSet {
            featureImage.setCornerRadius(16.0)
            featureImage.clipsToBounds = true
        }
    }
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var containerView: UIView! {
        didSet {
            containerView.setCornerRadius(8.0)
        }
    }
    public func bind(_ service: MService) {
        let imageUrl = URL(string: "https://lpn.boomtech.co/\(service.serviceImage)")!
        featureImage.sd_setImage(with: imageUrl, placeholderImage: UIImage())
        featureImage.sd_imageTransition = .fade(duration: 1.0)
        titleLabel.text = service.name
        subtitleLabel.text = service.description
    }
}
