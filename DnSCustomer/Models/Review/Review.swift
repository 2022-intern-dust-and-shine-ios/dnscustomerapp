//
//  Review.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 3/7/22.
//

import Foundation


struct InputReviews: Codable {
    var bookingId: Int
    var comment: String
    var rating: Double
    enum CodingKeys: String, CodingKey {
        case comment, rating
        case bookingId = "booking_id"
    }
}

struct OutputReviews: Codable {
    var data: RData
    var message: String
}

struct RData: Codable {
    var customer_id: Int
    var booking_id: Int
    var comment: String
    var rating: Double
    var updated_at: String
    var created_at: String
    var id: Int
    //var bookings: Bookings
}

struct Bookings: Codable {
    var id: Int
    var user_id: Int
    var company_id: Int
    var sched_datetime: String
    var start_datetime: String
    var end_datetime: String
    var address: String
    var latitude: Double?
    var longitude: Double?
    var status: Int
    var total: Double
    var created_at: String
    var updated_at: String
    var note: String?
}
