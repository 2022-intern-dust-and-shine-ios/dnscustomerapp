//
//  ProfilePresenter.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 2/2/22.
//

import Foundation
import UIKit


protocol ProfilePresenterDelegate: AnyObject {
    func showUserData(_ user: OutputUser)
    func showMessage(_ message: OutputChangePassword)
    func showError(statusCode: Int)
}

extension ProfilePresenterDelegate {
    func showUserData(_ user: OutputUser) {}
    func showMessage(_ message: OutputChangePassword) {}
}

class ProfilePresenter: NSObject {
    
    private weak var delegate: ProfilePresenterDelegate?
    private let customerService = CustomerService()
    
    init(delegate: ProfilePresenterDelegate) {
        self.delegate = delegate
    }
    
    public func getUserData(token: String) {
        customerService.getUser(token: token)  { [weak self] user, statusCode, error in
            guard let self = self else { return }
            guard let user = user, error == nil else {
                self.delegate?.showError(statusCode: statusCode ?? 0)
                return }
            self.delegate?.showUserData(user)
        }
    }
    public func changePassword(token: String, input: InputChangePassword) {
        customerService.postChangePassword(token: token, input: input) {
            [weak self] message, statusCode, error in
            guard let self = self else { return }
            guard let message = message, error == nil else {
                self.delegate?.showError(statusCode: statusCode ?? 0)
                return }
            self.delegate?.showMessage(message)
        }
    }
    public func updateUser(id: Int, token: String, input: InputUser) {
        customerService.update(id: id, token: token, input: input) {
            [weak self] user, statusCode, error in
            guard let self = self else { return }
            guard let user = user, error == nil else {
                self.delegate?.showError(statusCode: statusCode ?? 0)
                print(String(describing: error))
                return }
            self.delegate?.showUserData(user)
        }
    }
}
