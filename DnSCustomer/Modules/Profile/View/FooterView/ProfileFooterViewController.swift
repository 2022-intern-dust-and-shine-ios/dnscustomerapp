//
//  ProfileFooterViewController.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 1/14/22.
//

import UIKit

class ProfileFooterViewController: UIViewController {
    enum Constant {
        static let identifier = "FooterProfileCell"
    }
  
    @IBOutlet weak var footerTableView: UITableView!
    @IBOutlet weak var saveButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initViews()
        configureButtonandTableView()
    }
    private func initViews() {
        saveButton.setCornerRadius(saveButton.frame.height / 2)
        saveButton.titleLabel?.font = UIFont(name: "Calibri-Bold", size: 18.0)
    }
    private func configureButtonandTableView() {
        footerTableView.delegate = self
        footerTableView.dataSource = self

        footerTableView.tableFooterView = nil
        
        footerTableView.separatorInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)
    }
}

extension ProfileFooterViewController: UITableViewDataSource, UITableViewDelegate {
  
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Profiles.allCases.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let transact = Profiles.allCases[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: Constant.identifier, for: indexPath)
        cell.textLabel?.text = transact.rawValue
        cell.textLabel?.font = UIFont(name: "Calibri-Regular", size: 14)
        return cell
    }
    
    
}
