//
//  ResetPassword.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 3/31/22.
//

import Foundation


struct InputResetPassword: Codable {
    var token: String
    var email: String
    var password: String
    var password_confirmation: String
}

struct OutputResetPassword: Codable {
    var message: String
}
