//
//  SignUp.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 1/31/22.
//

import Foundation


// Input Model
struct InputRegister: Codable {

    var firstName: String
    var lastName: String
    var mobileNumber: String
    var email: String
    var password: String
    var passwordConfirmation: String
    var houseNumber: String
    var street: String
    var barangay: String
    var municipality: String
    var province: String
    var zipcode: String
    
    enum CodingKeys: String, CodingKey {
        case email, province, municipality
        case barangay, street, zipcode, password
        case firstName = "first_name"
        case lastName = "last_name"
        case mobileNumber = "mobile_number"
        case houseNumber = "house_number"
        case passwordConfirmation = "password_confirmation"
    }
}


// Output Model
struct OutputRegister: Codable {
    var tokenType: String
    var accessToken: String
    
    enum CodingKeys: String, CodingKey {
        case tokenType = "token_type"
        case accessToken = "access_token"
    }
}

