//
//  MessageTableViewCell.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 1/17/22.
//

import UIKit

class MyMessageTableViewCell: UITableViewCell {

    static let identifier: String = "MyMessageTableViewCell"
    
    @IBOutlet weak var messageContainerView: UIView! {
        didSet {
            let radius = CGFloat(15)
            self.messageContainerView.setCornerRadius(radius)
        }
    }
    @IBOutlet weak var messageLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public func bind(_ mes: Mes) {
        messageLabel.text = mes.message
    }
    
}
