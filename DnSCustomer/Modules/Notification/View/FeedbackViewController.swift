//
//  FeedbackViewController.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 1/26/22.
//

import UIKit

class FeedbackViewController: UIViewController {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var feedbackTextView: UITextView!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var ratingView: JStarRatingView! {
        didSet {
            ratingView.isUserInteractionEnabled = true
        }
    }
    
    let alertController = IVAlertController()
    
    //MARK: - PRESENTER
    private lazy var presenter = FeedbackPresenter(delegate: self)
    
    //MARK: - SERVICE
    private lazy var toolbarService = ToolbarService(delegate: self)
    
    private let userDefaults = UserDefaults.standard
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.setTitle("Feedback")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initViews()
    
        toolbarService.setupToolbar(textviews: [feedbackTextView], type: .textview)
        alertController.dismiss = { [weak self] in
            guard let self = self else { return }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.navigationController?.popViewController(animated: true)
            }
        }
        print(ratingView.rating)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        feedbackTextView.resignFirstResponder()
    }
    
    private func initViews() {
        containerView.layer.borderColor = AppColor.shadowColor.cgColor
        containerView.layer.borderWidth = 0.5
        containerView.setCornerRadius(4)
    
        submitButton.setCornerRadius(22.0)
    }
    
    private func redirectToDashboard() {
        guard let viewController = Modules.Main.initialViewController as? UITabBarController else {
            return
        }
        viewController.modalPresentationStyle = .fullScreen
        self.present(viewController, animated: true, completion: nil)
    }
  
    @IBAction func submitAction(_ sender: UIButton) {
        var rating: Float = 0.0
        if ratingView.rating > 5 {
            rating = 5.0
        } else if ratingView.rating < 0 {
            rating = 0.0
        } else {
            rating = ratingView.rating
        }
        if let comment = feedbackTextView.text,
            feedbackTextView.text.isNotEmpty() {
            
            if rating != 0.0 {
                presenter.postReview(token: userDefaults.getToken()!,
                                     input: .init(bookingId: userDefaults.getBookingId(),
                                                  comment: comment,
                                                  rating: Double(rating)))
            } else {
                self.showAlert(message: "Please give your ratings")
            }
    
        } else {
            print(rating)
            self.showAlert(message: "Please give your review")
        }
    }
}

extension FeedbackViewController: FeedbackPresenterDelegate {
    func showFeedbackOutput(_ output: OutputReviews) {
        print(output)
        alertController.showAlert(title: "Thank you, your response will help our company improved!",
                                  viewController: self.navigationController ?? self)
    }
}

extension FeedbackViewController: ToolbarProtocol {
    func dismissKeyboard() {
        self.view.endEditing(true)
    }
}
