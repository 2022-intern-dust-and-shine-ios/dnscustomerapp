//
//  Login.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 1/31/22.
//

import Foundation

//Input Model
struct InputLogin: Codable {
    var email: String
    var password: String
}



//Output Model
struct OutputLogin: Codable {
    var message: String
    var data: mData
}

struct mData: Codable {
    var user: User
    var token: String
}

struct User: Codable {
    var id: Int
    var firstName: String
    var lastName: String
    var mobileNumber: String
    var email: String
    var emailVerifiedAt: String?
    var createdAt: String
    var updatedAt: String
    var address : Address?
    var roles: [Roles]?
    enum CodingKeys: String, CodingKey {
        case id, email, address, roles
        case firstName = "first_name"
        case lastName = "last_name"
        case mobileNumber = "mobile_number"
        case emailVerifiedAt = "email_verified_at"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
}


struct Address: Codable {
    var id: Int
    var userId: Int
    var houseNumber: String
    var street: String
    var barangay: String
    var municipality: String
    var province: String
    var latitude: Double?
    var longitude: Double?
    var zipcode: String
    
    enum CodingKeys: String, CodingKey {
        case id, street, barangay, municipality, province, latitude, longitude, zipcode
        case userId = "user_id"
        case houseNumber = "house_number"
    }
}

struct Roles: Codable {
    var id: Int
    var name: String
    var guardName: String
    var createdAt: String
    var updatedAt: String
    var pivot: RPivot
    enum CodingKeys: String, CodingKey {
        case id, name, pivot
        case guardName = "guard_name"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
}

struct RPivot: Codable {
    var model_id: Int
    var role_id: Int
    var model_type: String
}
