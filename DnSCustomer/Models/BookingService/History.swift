//
//  History.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 3/7/22.
//

import Foundation

struct OutputHistory: Codable {
    var data : [HData]
    var message: String
}

struct HData: Codable {
    var id: Int
    var userId: Int
    var companyId: Int
    var schedDateTime: String
    var startDateTime: String?
    var endDateTime: String
    var address: String
    var latitude: Double?
    var longitude: Double?
    var status: Int
    var total: Double
    var paymentStatus: Int
    var createdAt: String
    var updatedAt: String
    var note: String?
    var services: HServices
    var reviews: Reviews?
    
    enum CodingKeys: String, CodingKey {
        case id, address, latitude, longitude, status, total
        case services, note, reviews
        case userId = "user_id"
        case companyId = "company_id"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case schedDateTime = "sched_datetime"
        case startDateTime = "start_datetime"
        case endDateTime = "end_datetime"
        case paymentStatus = "payment_status"
    }
}

typealias HServices = [HService]

struct HService: Codable {
    var id: Int
    var name: String
    var description: String
    var createdAt: String
    var updatedAt: String
    var time: String
    var price: Double
    var pivot: Pivot
    enum CodingKeys: String, CodingKey {
        case id, name, description, pivot, time, price
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
}

typealias Reviews = [Review]

struct Review: Codable {
    var id: Int
    var customerId: Int
    var bookingId: Int
    var comment: String
    var rating: Double
    var createdAt: String
    var updatedAt: String
    
    enum CodingKeys: String, CodingKey {
        case id, rating, comment
        case customerId = "customer_id"
        case bookingId = "booking_id"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
}
