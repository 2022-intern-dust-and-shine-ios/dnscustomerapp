//
//  CustomerService.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 2/11/22.
//

import Foundation
import Alamofire


struct CustomerService {
    
    public init() {}
    
    public func createBooking(_ input: InputCheckout, _ token: String, completion: @escaping CompletionHandler<OutputCheckout>) {
        
        AF.request(CustomerAPI.checkout(input, token)).responseDecodable(of: OutputCheckout.self)
        { response in
            let statusCode = response.response?.statusCode
            switch response.result {
            case .success(let checkout):
                completion(checkout, statusCode, nil)
            case .failure(let error):
                print(String(describing: error))
                completion(nil, statusCode, error)
            }
        }
    }
    
    public func getBookingService(_ token: String,
                                  completion: @escaping CompletionHandler<OutputBookService>) {
        AF.request(CustomerAPI.bookingservice(token)).responseDecodable(of: OutputBookService.self)
        { response in
            let statusCode = response.response?.statusCode
            switch response.result {
            case .success(let bookservice):
                completion(bookservice,statusCode, nil)
            case .failure(let error):
                print(String(describing: error))
                completion(nil, statusCode, error)
            }
        }
        .cURLDescription { description in
            print(description)
        }
    }
    
    public func getHistory(_ token: String, completion: @escaping CompletionHandler<OutputHistory>) {
        AF.request(CustomerAPI.history(token)).responseDecodable(of: OutputHistory.self)
        { response in
            let statusCode = response.response?.statusCode
            switch response.result {
            case .success(let history):
                completion(history, statusCode, nil)
            case .failure(let error):
                completion(nil, statusCode, error)
            }
        }
    }
    public func psotReview(token: String, input: InputReviews, completion: @escaping CompletionHandler<OutputReviews>) {
        
        AF.request(CustomerAPI.reviews(token, input)).responseDecodable(of: OutputReviews.self) { response in
            let statusCode = response.response?.statusCode
            switch response.result {
            case .success(let reviews):
                completion(reviews, statusCode, nil)
            case .failure(let error):
                print(String(describing: error))
                print(error.localizedDescription)
                completion(nil, statusCode, error)
            }
        }
    }
    
    public func getNotifications(token: String, completion: @escaping CompletionHandler<OutputNotification>) {
        AF.request(CustomerAPI.notification(token)).responseDecodable(of: OutputNotification.self) { response in
            let statusCode = response.response?.statusCode
            switch response.result {
            case .success(let notification):
                completion(notification, statusCode, nil)
            case .failure(let error):
                completion(nil, statusCode, error)
            }
        }
    }
    
    public func update(id: Int, token: String, input: InputUser, completion: @escaping AuthServiceCompletionHandler<OutputUser>) {
        AF.request(CustomerAPI.update(id, token, input)).responseDecodable(of: OutputUser.self)
        { response in
            let statusCode = response.response?.statusCode
            switch response.result {
            case .success(let user):
                completion(user, statusCode, nil)
            case .failure(let error):
                print(String(describing: error))
                print(error.localizedDescription)
                completion(nil, statusCode, error)
            }
        }
        .cURLDescription { description in
            print(description)
        }
    }
    
    public func getUser(token: String, completion: @escaping AuthServiceCompletionHandler<OutputUser>) {
        AF.request(CustomerAPI.user(token)).responseDecodable(of: OutputUser.self)
        { response in
            let statusCode = response.response?.statusCode
            switch response.result {
            case .success(let user):
                completion(user, statusCode, nil)
            case .failure(let error):
                print(String(describing: error))
                completion(nil, nil, error)
            }
        }
    }
    public func postChangePassword(token: String, input: InputChangePassword, completion: @escaping AuthServiceCompletionHandler<OutputChangePassword>) {
        
        AF.request(CustomerAPI.change_password(token, input)).responseDecodable(of: OutputChangePassword.self) { response in
            let statusCode = response.response?.statusCode
            
            switch response.result {
            case .success(let message):
                completion(message, statusCode, nil)
            case .failure(let error):
                print(String(describing: error))
                completion(nil, statusCode, error)
            }
        }
    }
    
    public func postForgotPassword(input: InputForgotPassword, completion: @escaping CompletionHandler<OutputForgotPassword>) {
        AF.request(CustomerAPI.forgot_password(input)).responseDecodable(of: OutputForgotPassword.self) { response in
            let statusCode = response.response?.statusCode
            
            switch response.result {
            case .success(let output):
                completion(output, statusCode, nil)
            case .failure(let error):
                print(String(describing: error))
                completion(nil, statusCode, error)
            }
        }
        .cURLDescription { description in
            print(description)
        }
    }
    
    public func postResetPassword(input: InputResetPassword, completion: @escaping CompletionHandler<OutputResetPassword>) {
        AF.request(CustomerAPI.reset_password(input)).responseDecodable(of: OutputResetPassword.self) { response in
            let statusCode = response.response?.statusCode
            switch response.result {
            case .success(let output):
                completion(output, statusCode, nil)
            case .failure(let error):
                print(String(describing: error))
                completion(nil, statusCode, error)
            }
        }
    }
}
