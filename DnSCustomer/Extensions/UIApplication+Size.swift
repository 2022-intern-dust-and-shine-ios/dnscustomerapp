//
//  UIApplication+Size.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 3/16/22.
//

import Foundation

import UIKit

//extension UIApplication {
//    static var statusBarHeight: CGFloat {
//        if #available(iOS 13.0, *) {
//            let window = shared.windows.filter { $0.isKeyWindow }.first
//            return window?.windowScene?.statusBarManager?.statusBarFrame.height ?? 0
//        }
//        return shared.statusBarFrame.height
//    }
//}

struct ScreenUtils {
    static var width: CGFloat {
        return UIScreen.main.bounds.width
    }
    
    static var height: CGFloat {
        return UIScreen.main.bounds.height
    }
//
//    static var statusBarHeight: CGFloat {
//        return UIApplication.statusBarHeight
//    }
}


