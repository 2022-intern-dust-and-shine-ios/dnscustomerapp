//
//  ToolbarProtocol.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 3/23/22.
//

import Foundation
import UIKit

enum InputType {
    case all
    case textfield
    case textview
}

protocol ToolbarProtocol: AnyObject {
    func dismissKeyboard()
}

class ToolbarService: NSObject {
    
    private weak var delegate: ToolbarProtocol?
  
    init(delegate: ToolbarProtocol) {
        self.delegate = delegate
    }
    
    public func setupToolbar(textfields: [UITextField] = [],
                             textviews: [UITextView] = [],
                             type: InputType = .textfield) {
        let bar = UIToolbar()
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(dismissedKeyboard))
        bar.items = [flexSpace,done]
        bar.sizeToFit()
        switch type {
        case .all:
            break
        case .textfield:
            textfields.forEach { $0.inputAccessoryView = bar }
        case .textview:
            textviews.forEach { $0.inputAccessoryView = bar }
        }
 
    }
    
    @objc func dismissedKeyboard() {
        delegate?.dismissKeyboard()
    }
}
