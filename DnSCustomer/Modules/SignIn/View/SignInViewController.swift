//
//  SignInViewController.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 1/14/22.
//

import UIKit


class SignInViewController: UIViewController {
    
    //MARK: - IBOUTLETS
    @IBOutlet weak var containerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var usernameemailContainerView: UIView!
    @IBOutlet weak var passwordContainerView: UIView!
    @IBOutlet weak var baseContainerView: UIView!
    @IBOutlet weak var usernameemailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var signinButton: UIButton!
    @IBOutlet weak var forgotPassButton: UIButton!
    @IBOutlet weak var createAccntButton: UIButton!
    @IBOutlet weak var constraintLayout: NSLayoutConstraint!
    @IBOutlet weak var showPassButton: UIButton!
    @IBOutlet weak var showPassConstraint: NSLayoutConstraint!
    
    private var loader: IVActivityIndicator?

    private let userDefaullts = UserDefaults.standard
    
    //MARK: - PROPERTIES
    private var kbSize: CGFloat = 216.0

    //MARK: - PRESENTER
    private lazy var presenter = LoginPresenter(delegate: self)
    
    //MARK: - SERVICE
    private lazy var keyboardService = KeyboardService(delegate: self)
    
    //MARK: - APP LIFECYCLE
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.setSyle(.hidden)
        self.navigationItem.backButtonTitle = ""
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loader = IVActivityIndicator(superView: self)
        setupViews()
        setupTextfieldsAndButtons()
        
        containerHeightConstraint.constant = self.view.bounds.width > 375 ? 745 : 584
        
        setupService()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        [usernameemailTextField,
         passwordTextField].forEach { $0?.resignFirstResponder() }
        constraintLayout.constant = 30
    }
    
    private func setupViews() {
        self.view.backgroundColor = AppColor.blueColor
        
        [usernameemailContainerView,
         passwordContainerView,
         baseContainerView].forEach { $0?.setCornerRadius(8.0) }
        baseContainerView.addShadow()
        signinButton.setCornerRadius(signinButton.frame.height / 2)
    }
    private func setupTextfieldsAndButtons() {
        signinButton.addTarget(self, action: #selector(self.signInAction(sender:)), for: .touchUpInside)
        forgotPassButton.addTarget(self, action: #selector(self.forgotPassAction), for: .touchUpInside)
        
        usernameemailTextField.delegate = self
        passwordTextField.delegate = self
    }
    
    private func setupService() {
        keyboardService.setupRegisterKbNotification()
    }
    
    private func resignTextFields() {
        [usernameemailTextField,
         passwordTextField].forEach {
            $0?.text = ""
            $0?.resignFirstResponder()
        }
    }
   
    private func inputValidation(usernameemail: String?, password: String?) -> Bool {
        
        if usernameemail != nil && usernameemail!.isNotEmpty() {
            if password != nil && password!.isNotEmpty() {
                if password!.count >= 8 {
                    return true
                } else {
                    self.showAlert(message: "The password must be at least 8 characters and above")
                    return false
                }
            } else {
             
                self.showAlert(message: "Invalid Password")
                passwordTextField.showError(passwordContainerView)
                showPassButton.isHidden = true
                showPassConstraint.constant = 0
                return false
            }
        } else {
           
            self.showAlert(message: "Invalid Email")
            usernameemailTextField.showError(usernameemailContainerView)
            return false
        }
    }
    

    private func redirectToDashboard() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            self.loader?.stopLoader()
            guard let viewController = Modules.Main.initialViewController as? UITabBarController else {
                return
            }
            
            viewController.modalPresentationStyle = .fullScreen
            self.present(viewController, animated: true, completion: nil)
        }
    }
    
    //MARK: - ACTIONS
    @objc func signInAction(sender: UIButton) {
        sender.haptic()
        let usernameemail = usernameemailTextField.text
        let password = passwordTextField.text
        let isValid = inputValidation(usernameemail: usernameemail, password: password)
        
        if isValid {
            if Connectivity.isConnectedToInternet() {
                loader?.showLoader(navController: self.navigationController!)
                userDefaullts.setPassword(value: password!)
                presenter.setLogin(email: usernameemail!, password: password!)
            } else {
                self.showAlert(actionTitle: "Okay", message: "Please check your internet connection")
            }
        }
    }

    @objc func forgotPassAction() {
        NotificationCenter.default.removeObserver(self)
        resignTextFields()
        guard let viewController = Modules.Forgot.initialViewController as? VerifyEmailViewController else {
            return
        }
        viewController.modalPresentationStyle = .overCurrentContext
        viewController.signinviewController = self
        present(viewController, animated: false, completion: nil)
    }
    
    @IBAction func createAccountAction(_ sender: UIButton) {
        sender.haptic()
        resignTextFields()
        guard let viewController = Modules.SignUp.initialViewController as? SignUpViewController else { return }
       
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func showhideAction(_ sender: UIButton) {
        let showPass = UIImage(systemName: "eye.fill")
        let hidePass = UIImage(systemName: "eye.slash.fill")
        if sender.isSelected {
            sender.isSelected = false
            passwordTextField.isSecureTextEntry = true
        } else {
            passwordTextField.isSecureTextEntry = false
            sender.isSelected = true
        }
        showPassButton.setImage(sender.isSelected ? showPass : hidePass,
                                for: .normal)
    }
}

//MARK: - CONTROLLER EXTENSION
extension SignInViewController: UITextFieldDelegate {
 
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        constraintLayout.constant = 10
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        switch textField.tag {
        case 1:
            textField.removeError(usernameemailContainerView)
        case 2:
            textField.removeError(passwordContainerView)
            showPassButton.isHidden = false
            showPassConstraint.constant = 27
        default:
            break
        }
        return true
    }
}


extension SignInViewController: LoginPresenterDelegate {

    func presentOutputable(output: OutputLogin, _ statusCode: Int) {
        
        switch statusCode {
        case 200:
            guard let role = output.data.user.roles?.first else { return }
            if role.name == CustomerRole.role {
                self.userDefaullts.setFirstletter(value: String(output.data.user.firstName.first  ?? "A"))
                self.userDefaullts.setToken(value: output.data.token)
                self.resignTextFields()
                self.redirectToDashboard()
        
            } else {
                loader?.stopLoader()
                self.showAlert(title: "Invalid Account",
                               message: "Please enter your valid email and password")
                
            }
        case 401:
            self.showAlert(message: "Invalid Credentials")
        default:
            break
        }
    }
    
    func presentError(_ error: String, _ statusCode: Int) {
        switch statusCode {
        case 401:
            self.showAlert(message: "Invalid Credentials")
        default:
            self.showAlert(message: "Invalid email or password")
        }
        loader?.stopLoader()
    }
}

extension SignInViewController: KeyboardProtocol {
    func getKeyboardSize(size: CGSize) {
        constraintLayout.constant = size.height + 10
    }    
}
