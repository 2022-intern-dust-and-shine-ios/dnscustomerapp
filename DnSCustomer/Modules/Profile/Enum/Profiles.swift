//
//  Transactions.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 1/14/22.
//

import Foundation


enum Profiles: String, CaseIterable {
    case personal_info = "Personal Information"
    case manage_bank_account = "Manage Bank Account"
    case manage_creditdebit_card = "Manage Credit/Debit Card"
    case logout = "Logout"
}
