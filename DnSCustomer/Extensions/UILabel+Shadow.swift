//
//  UILabel+Shadow.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 1/19/22.
//

import Foundation
import UIKit

extension UILabel {
    
    func dropShadow() {
        layer.shadowColor = AppColor.shadowColor.cgColor
        layer.shadowRadius = 4.0
        layer.shadowOpacity = 0.8
        layer.shadowOffset = CGSize(width: 4, height: 4)
        layer.masksToBounds = false
    }
    
}
