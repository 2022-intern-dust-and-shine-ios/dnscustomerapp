//
//  UIButton+State.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 4/4/22.
//

import Foundation

import UIKit

extension UIButton {
    func enabled() {
        self.isHidden = false
        self.isEnabled = true
    }
    func disabled() {
        self.isHidden = true
        self.isEnabled = false
    }
}
