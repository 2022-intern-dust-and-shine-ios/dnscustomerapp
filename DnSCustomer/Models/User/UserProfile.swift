//
//  OutputUser.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 2/8/22.
//

import Foundation

struct InputUser: Codable {
    var firstName: String
    var lastName: String
    var mobileNumber: String
    var email: String
    var houseNumber: Int
    var street: String
    var barangay: String
    var municipality: String
    var province: String
    var zipcode: String
    
    enum CodingKeys: String, CodingKey {
        case email, province, municipality
        case barangay, street, zipcode
        case firstName = "first_name"
        case lastName = "last_name"
        case mobileNumber = "mobile_number"
        case houseNumber = "house_number"
    }
}

struct OutputUser: Codable {
    var data: [User]
    var message: String
    enum CodingKeys: String, CodingKey {
        case data, message
    }
}

