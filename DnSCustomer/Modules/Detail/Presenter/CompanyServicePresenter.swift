//
//  CompanyDetailPresenter.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 2/9/22.
//

import Foundation


protocol CompanyPresenterDelegate: AnyObject {
    func showCompany(_ company: Company<MCompany>)
        
    func showError(_ error: String)
    
    func showCompanies(company: Company<Companies>)
    
    func showFilteredCompanies(company: Company<Companies>)
    
}

extension CompanyPresenterDelegate {
    func showCompany(_ company: Company<MCompany>) {}
    func showCompanies(company: Company<Companies>) {}
    func showFilteredCompanies(company: Company<Companies>) {}
}

class CompanyPresenter: NSObject {
    
    private weak var delegate: CompanyPresenterDelegate?
    private let companyService = CompanyService()
    
    init(delegate: CompanyPresenterDelegate) {
        self.delegate = delegate
    }
    
    public func getCompany(_ token: String, _ id: Int) {
        companyService.getCompany(id: id, token: token) { [weak self] company, statusCode, error in
            guard let self = self else { return }
            guard let company = company, error == nil else { return }
            self.delegate?.showCompany(company)
        }
    }
    
    public func getListOfCompanies(_ token: String) {
        companyService.getCompanies(token: token) { [weak self] company, statusCode, error in
            guard let self = self else { return }
            guard let company = company, error == nil else {
                print(String(describing: error))
                return }
            self.delegate?.showCompanies(company: company)
        }
    }
    
    public func getSearchCompanies(_ keyword: String, _ token: String) {
        companyService.searchCompanies(keyword: keyword, token: token) { [weak self] company, statusCode, error in
            guard let self = self else { return }
            guard let company = company, error == nil else { return }
            self.delegate?.showFilteredCompanies(company: company)
        }
    }
}
