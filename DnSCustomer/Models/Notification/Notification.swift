//
//  Notification.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 3/7/22.
//

import Foundation



struct OutputNotification: Codable {
    
    var data: [NData]
    var message: String
}

struct NData: Codable {
    var id: Int
    var userId: Int
    var companyId: Int
    var schedDateTime: String
    var startDateTime: String
    var endDateTime: String
    var address: String
    var latitude: Double?
    var longitude: Double?
    var status: Int
    var total: Double
    var createdAt: String
    var updatedAt: String
    var note: String?
    var services: HServices
    var workers: NWorkers
    var reviews: Reviews
    enum CodingKeys: String, CodingKey {
        case id, address, latitude, longitude, status, total, note
        case services, workers, reviews
        case userId = "user_id"
        case companyId = "company_id"
        case schedDateTime = "sched_datetime"
        case startDateTime = "start_datetime"
        case endDateTime = "end_datetime"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        
    }
}

typealias NWorkers = [NWorker]

struct NWorker: Codable {
    var id: Int
    var firstName: String
    var lastName: String
    var mobileNumber: String
    var email: String
    var emailVerifiedAt: String?
    var createdAt: String
    var updatedAt: String
    var pivot: NPivot
    enum CodingKeys: String, CodingKey {
        case id, email, pivot
        case firstName = "first_name"
        case lastName = "last_name"
        case mobileNumber = "mobile_number"
        case emailVerifiedAt = "email_verified_at"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
}

struct NPivot: Codable {
    var bookingId: Int
    var userId: Int
    enum CodingKeys: String, CodingKey {
        case bookingId = "booking_id"
        case userId = "user_id"
    }
}
