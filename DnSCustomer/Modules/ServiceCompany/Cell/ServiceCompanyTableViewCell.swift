//
//  ServiceCompanyTableViewCell.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 3/1/22.
//

import UIKit

class ServiceCompanyTableViewCell: UITableViewCell {

    static let identifier: String = "ServiceCompanyTableViewCell"
    @IBOutlet weak var containerView: UIView! {
        didSet {
            containerView.lightShadow()
            containerView.setCornerRadius(8)
        }
    }
    @IBOutlet weak var companyImageView: UIImageView! {
        didSet {
            companyImageView.setCornerRadius(8)
            companyImageView.clipsToBounds = true
        }
    }
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var ratingsLabel: UILabel!
    @IBOutlet weak var ratingContainerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        ratingContainerView.setCornerOutline(4.0)
        ratingContainerView.isHidden = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public func bind(_ company: MCompany) {
        nameLabel.text = company.name
        addressLabel.text = company.address
        
        let urlImage = URL(string: "https://lpn.boomtech.co/\(company.companyImage)")
        companyImageView.sd_setImage(with: urlImage, placeholderImage: UIImage(), options: .refreshCached)
        companyImageView.sd_imageTransition = .fade(duration: 2.0)
    }
}

