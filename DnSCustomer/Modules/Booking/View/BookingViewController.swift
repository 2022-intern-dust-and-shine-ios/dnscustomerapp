//
//  BookingViewController.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 1/18/22.
//

import UIKit



class BookingViewController: UIViewController {

    @IBOutlet weak var bookingTableView: UITableView!
    
    //MARK: PRESENTER
    private lazy var presenter = BookingServicePresenter(delegate: self)
    
    //MARK: PROPERTIES
    private var bookservices: CDATAs = [] {
        didSet {
            bookingTableView.reloadData()
            
            if bookservices.count != 0 {
                bookingTableView.backgroundView = nil
            } else {
                bookingTableView.empty(.service)
            }
        }
    }
    private var refreshControl = UIRefreshControl()
    
    private var loaderIndicatorView : UIActivityIndicatorView = {
        let loader = UIActivityIndicatorView()
        loader.hidesWhenStopped = true
        loader.style = .medium
        return loader
    }()
    
    private let userDefault = UserDefaults.standard
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.setTitle("Booked Service")
        self.navigationItem.backButtonTitle = ""
        self.navigationController?.navigationBar.setSyle(.white)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureTableView()
        startLoader()
        setupRefreshControl()
        
        if Connectivity.isConnectedToInternet() {
            presenter.getBookingService(token: userDefault.getToken()!)
        } else {
            self.showAlert(actionTitle: "Okay", message: "Please check your internet connection")
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
            
        refreshControl.endRefreshing()
        stopLoader()
    }
    
    private func configureTableView() {
        bookingTableView.delegate = self
        bookingTableView.dataSource = self
        
        bookingTableView.separatorStyle = .none
    }
    
    private func setupRefreshControl() {
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        bookingTableView.addSubview(refreshControl)
    }
    
    private func startLoader() {
        loaderIndicatorView.frame = CGRect(origin: .zero,
                                           size: .init(width: 34, height: 34))
        loaderIndicatorView.startAnimating()
        bookingTableView.tableHeaderView = loaderIndicatorView
    }
    private func stopLoader() {
        self.loaderIndicatorView.stopAnimating()
        self.bookingTableView.tableHeaderView = nil
    }
    private func redirectToModal(data: CData) {
        guard let viewController = storyboard?.instantiateViewController(withIdentifier: "BDModalViewController") as? BDModalViewController else { return }
        viewController.dataService = data
        viewController.type = .booking_service
        viewController.modalPresentationStyle = .overCurrentContext
        present(viewController, animated: false, completion: nil)
        
    }
    @IBAction func historyAction(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "BookingHistoryViewController") as! BookingHistoryViewController
               vc.hidesBottomBarWhenPushed = true
               self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func refresh(_ sender: AnyObject) {
       // Code to refresh table view
        if Connectivity.isConnectedToInternet() {
            presenter.getBookingService(token: userDefault.getToken()!)
        } else {
            self.showAlertWithCompletion(actionTitle:"Okay",
                                         message: "Please check your internet connection") {
                self.refreshControl.endRefreshing()
            }
        }
        
    }
}

extension BookingViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bookservices.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: BookingTableViewCell.identifier, for: indexPath) as? BookingTableViewCell {
            cell.bind(bookservices[indexPath.row],
                      letter: userDefault.getFirstletter())
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if ModalFeatureFlag.flag {
            redirectToModal(data: bookservices[indexPath.row])
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(100.0)
    }
}

extension BookingViewController: BookingServicePresenterDelegate {
    func showError(_ error: String) {
        stopLoader()
        self.showAlert(message: error)
    }
    
    func showBookService(bookservice: OutputBookService) {
        stopLoader()
        var services: CDATAs = []
        services = bookservice.data.sorted { lhs, rhs in
            return lhs.id > rhs.id
        }
        bookservices = services
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.refreshControl.endRefreshing()
        }
    }
}
