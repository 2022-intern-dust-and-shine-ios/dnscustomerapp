//
//  ServiceCompanyPresenter.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 3/1/22.
//

import Foundation

protocol ServiceCompanyDelegate: AnyObject {
    func showCompaniesByService(companies: Company<Companies>)
}

class ServiceCompanyPresenter: NSObject {
    
    private let delegate: ServiceCompanyDelegate?
    private let companyService = CompanyService()
    
    init(delegate: ServiceCompanyDelegate) {
        self.delegate = delegate
    }
    
    public func getCompaniesByService(token: String, id: Int) {
        companyService.filterCompaniesByService(id: id, token: token)
        { [weak self] companies, statusCode, error in
            guard let self = self else { return }
            guard let companies = companies, error == nil else { return }
            self.delegate?.showCompaniesByService(companies: companies)
        }
    }
    
}
