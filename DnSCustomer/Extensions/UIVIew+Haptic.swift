//
//  UIVIew+Haptic.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 3/29/22.
//

import Foundation
import UIKit
import AudioToolbox

extension UIView {
    func haptic() {
        guard let device = UIDevice.current.value(forKey: "_feedbackSupportLevel") as? Int else {
            print("NIL DEVICE")
            return
        }
        print(device)
        switch device {
        case 0 :
            break
        case 1:
            AudioServicesPlaySystemSound(1519) //Actuate `Nope` feedback (series of three weak booms)
        case 2:
            let generator = UIImpactFeedbackGenerator(style: .light)
            generator.prepare()
            generator.impactOccurred()
        default:
            break
        }
    }
}
