//
//  IVActivityIndicator.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 1/17/22.
//

import Foundation
import UIKit



struct IVActivityIndicator {
    
    private var superView: UIViewController?
    private var navView: UIView?
    init(superView: UIViewController = UIViewController()) {
        self.superView = superView
    }
    

    private var activityIndicator : UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView()
        indicator.translatesAutoresizingMaskIntoConstraints = false
        indicator.style = UIActivityIndicatorView.Style.medium
        indicator.tintColor = .darkGray
        indicator.color = .white
        return indicator
    }()
    private var backgroundView: UIView = {
        let view = UIView()
        return view
    }()
    
  //TODO: - Refactor this
    ///Parameter - showloadere(navController: UINavigationController) - done
    mutating func showLoader(navController: UIViewController?) {
        self.navView = navController?.view
        navView?.isUserInteractionEnabled = false
        backgroundView.frame = superView!.view.bounds
        superView!.view.addSubview(backgroundView)
        backgroundView.addBlurredBackground(style: .dark)
        backgroundView.addSubview(activityIndicator)
        navController?.view.addSubview(backgroundView)
        activityIndicator.centerXAnchor.constraint(equalTo: backgroundView.centerXAnchor)
            .isActive = true
        activityIndicator.centerYAnchor.constraint(equalTo: backgroundView.centerYAnchor)
            .isActive = true

        UIView.animate(withDuration: 0.25) { [weak backgroundView] in
            guard let backgroundView = backgroundView else { return }
            backgroundView.alpha = 1.0
        }

        activityIndicator.startAnimating()
    }
    
    func stopLoader() {
        navView?.isUserInteractionEnabled = true
        self.activityIndicator.stopAnimating()
        self.backgroundView.alpha = 0.0
        self.backgroundView.removeFromSuperview()
        self.activityIndicator.removeFromSuperview()
    }
}

extension UIView {
    func addBlurredBackground(style: UIBlurEffect.Style) {
        let blurEffect = UIBlurEffect(style: style)
        let blurView = UIVisualEffectView(effect: blurEffect)
        blurView.frame = self.frame
        blurView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(blurView)
        self.sendSubviewToBack(blurView)
    }
}
