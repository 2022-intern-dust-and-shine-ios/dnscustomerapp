//
//  UIViewController+Alert.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 3/22/22.
//

import Foundation

import UIKit

enum AlertActionType {
    case normal
    case logout
}

extension UIViewController {
    
     func showAlert(title: String = "Unable to proceed",
                          actionTitle: String = "Try Again" ,
                          message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
         alert.addAction(UIAlertAction(title: actionTitle, style: .default, handler: nil))

        present(alert, animated: true, completion: nil)
    }
    func showAlertWithCompletion(title: String = "Unable to proceed",
                                 actionTitle: String = "Try Again" ,
                                 message: String, type: AlertActionType = .normal,
                                 completion: @escaping() -> Void = {}) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
     
        switch type {
        case .normal:
            let action = UIAlertAction(title: actionTitle, style: .cancel) { (_) in
                completion()
            }
            alert.addAction(action)
        case .logout:
            alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
            alert.addAction(UIAlertAction(title: "Yes", style: .default) { (_) in
                completion()
            })
        }
        present(alert, animated: true, completion: nil)
    }
    
}
