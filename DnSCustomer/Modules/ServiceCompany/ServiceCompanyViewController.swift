//
//  ServiceCompanyViewController.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 2/28/22.
//

import UIKit

class ServiceCompanyViewController: UIViewController {

    @IBOutlet weak var serviceCompanyTableView: UITableView!
    
    private lazy var presenter = ServiceCompanyPresenter(delegate: self)
    
    private let userDefaults = UserDefaults.standard
    
    var id: Int?
    
    var companies: Companies = [] {
        didSet {
            serviceCompanyTableView.reloadData()
            
            if companies.count != 0 {
                serviceCompanyTableView.backgroundView = nil
            } else {

                serviceCompanyTableView.empty(.company)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.setTitle(userDefaults.getCompanyName())
        self.navigationItem.backButtonTitle = ""
        navigationController?.navigationBar.setSyle(.transparent)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureTableView()
        presenter.getCompaniesByService(token: userDefaults.getToken()!, id: id ?? 0)
    }

    private func configureTableView() {
        serviceCompanyTableView.delegate = self
        serviceCompanyTableView.dataSource = self
        
        serviceCompanyTableView.separatorStyle = .none
    }
    
    private func redirectToDetailCompany() {
        guard let viewController = Modules.Detail.initialViewController as? CompanyDetailViewController else { return }
        viewController.hidesBottomBarWhenPushed = true

        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

extension ServiceCompanyViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return companies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: ServiceCompanyTableViewCell.identifier, for: indexPath) as? ServiceCompanyTableViewCell {
            cell.bind(companies[indexPath.row])
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(199.0)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        userDefaults.setCompanyRating(value: 0.0)
        userDefaults.setCompanyId(value: companies[indexPath.row].id)
        userDefaults.setCompanyName(value: companies[indexPath.row].name)
        redirectToDetailCompany()
    }
}

extension ServiceCompanyViewController: ServiceCompanyDelegate {
    func showCompaniesByService(companies: Company<Companies>) {
        self.companies = companies.data
    }
}
