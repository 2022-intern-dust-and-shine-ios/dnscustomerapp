//
//  RecommendationCV.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 1/19/22.
//

import UIKit
import SDWebImage

class RecommendationCVC: UICollectionViewCell {
    
    static let identifier: String = "RecommendationCVC"
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var companyImage: UIImageView!
    @IBOutlet weak var companynameLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var ratings: UILabel!
    @IBOutlet weak var ratingContainerView: UIView!
    override func awakeFromNib() {
        addCellShadow()
    }
    
    func addCellShadow() {
        companyImage.setCornerRadius(8.0)
        companyImage.clipsToBounds = true
        containerView.setCornerRadius(8.0)
        ratingContainerView.setCornerOutline(4.0)
        [companynameLabel,
         locationLabel].forEach { $0?.dropShadow()}
    }
    
    public func bind(_ company: RecommendedCompany) {
        let urlImage = URL(string: "https://lpn.boomtech.co/\(company.company.companyImage)")
        companynameLabel.text = company.company.name
        locationLabel.text = company.company.address
        ratings.text = "\(company.rating)"
        companyImage.sd_setImage(with: urlImage, placeholderImage: UIImage(), options: .refreshCached)
        companyImage.sd_imageTransition = .fade(duration: 1.0)
    }
}
