//
//  NewPasswordViewController.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 1/18/22.
//

import UIKit
import SafariServices

class ResetPasswordViewController: UIViewController {
 
    

    //MARK: - IBOUTLETS
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var emailaddressLabel: UILabel!
    @IBOutlet weak var newpasswordTextField: UITextField!
    @IBOutlet weak var retypepasswordTextField: UITextField!
    @IBOutlet weak var resetcodeTextField: UITextField!
    
    @IBOutlet weak var showhideButton1: UIButton!
    @IBOutlet weak var showhideButton2: UIButton!
    @IBOutlet weak var showhideButton3: UIButton!
    @IBOutlet weak var newpassContainerView: UIView!
    @IBOutlet weak var retypepassContainerView: UIView!
    @IBOutlet weak var resetcodeContainerView: UIView!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var constraintLayout: NSLayoutConstraint!
    
    //MARK: - PRESENTER
    private lazy var presenter = ResetPasswordPresenter(delegate: self)
    
    private var loader: IVActivityIndicator?
    var alertController = IVAlertController()
    
    var email: String?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.setTitle("Forgot Password")
        self.navigationController?.navigationBar.setSyle(.white)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loader = IVActivityIndicator(superView: self)
        

        
        setupDismissController()
        
        setupEmail()
        initViews()
        configureTextFields()
    }

    private func setupEmail() {
        emailaddressLabel.text = self.email ?? ""
    }
    
    private func setupDismissController() {
        alertController.dismiss = {
            [weak self] in
            guard let self = self else { return }
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    private func initViews() {
      
        let radius = userImageView.frame.size.width / 2
        userImageView.setCornerRadius(radius)
        userImageView.clipsToBounds = true
       
        
        saveButton.setCornerRadius(6)
        let views = [
            newpassContainerView,
            retypepassContainerView,
            resetcodeContainerView
        ]
        
        views.forEach {
            $0?.lightShadow()
            $0?.setCornerRadius(6)
        }
    }
    

    private func redirectToSite() {
        if let url = URL(string: "https://mailtrap.io/inboxes/1682305/messages") {
            let config = SFSafariViewController.Configuration()
            config.entersReaderIfAvailable = true
            
            let vc = SFSafariViewController(url: url, configuration: config)
            present(vc, animated: true, completion: nil)
        }
    }
    
    private func inputValidation(newPass: String?,
                                 confirmPass: String?,
                                 code: String?) -> Bool {
        if newPass != nil && newPass!.isNotEmpty() {
            if confirmPass != nil && confirmPass!.isNotEmpty() {
                if code != nil && code!.isNotEmpty() {
                    return true
                } else {
              
                    self.showAlert(message: "Reset Code is empty")
                    resetcodeTextField.showError(resetcodeContainerView)
                    showhideButton3.isHidden = true
                    return false
                }
            } else {
            
                self.showAlert(message: "Confirm Password is empty")
                retypepasswordTextField.showError(retypepassContainerView)
                showhideButton2.isHidden = true
                return false
            }
        } else {
          
            self.showAlert(message: "New Password is empty")
            newpasswordTextField.showError(newpassContainerView)
            showhideButton1.isHidden = true
            return false
        }
    }
    
    private func configureTextFields() {
        [newpasswordTextField,
        retypepasswordTextField,
         resetcodeTextField].forEach {
            $0?.delegate = self
        }
    }
    
    //MARK: - IBACTIONS
    @IBAction func showhideActions(_ sender: UIButton) {
        let showPass = UIImage(systemName: "eye.fill")
        let hidePass = UIImage(systemName: "eye.slash.fill")
        switch sender.tag {
        case 1:
            if sender.isSelected {
                sender.isSelected = false
                newpasswordTextField.isSecureTextEntry = true
            } else {
                sender.isSelected = true
                newpasswordTextField.isSecureTextEntry = false
            }
            showhideButton1.setImage(sender.isSelected ? showPass : hidePass, for: .normal)
        case 2:
            if sender.isSelected {
                sender.isSelected = false
                retypepasswordTextField.isSecureTextEntry = true
            } else {
                sender.isSelected = true
                retypepasswordTextField.isSecureTextEntry = false
            }
            showhideButton2.setImage(sender.isSelected ? showPass : hidePass, for: .normal)

        case 3:
            if sender.isSelected {
                sender.isSelected = false
                resetcodeTextField.isSecureTextEntry = true
            } else {
                sender.isSelected = true
                resetcodeTextField.isSecureTextEntry = false
            }
            showhideButton3.setImage(sender.isSelected ? showPass : hidePass, for: .normal)

        default:
            break
        }
    }
    
    @IBAction func saveAction(_ sender: UIButton) {
        
        let newPass = newpasswordTextField.text
        let confimPass = retypepasswordTextField.text
        let resetcode = resetcodeTextField.text
        let isValid = inputValidation(newPass: newPass,
                                      confirmPass: confimPass,
                                      code: resetcode)
        if isValid {
            loader?.showLoader(navController: navigationController ?? self)
            presenter.postResetPassword(input: .init(token: resetcode!,
                                                     email: self.email ?? "",
                                                     password: newPass!,
                                                     password_confirmation: confimPass!))
        }
    }
}

extension ResetPasswordViewController: ResetPasswordPresenterDelegate {
    func resetPassword(output: OutputResetPassword, statusCode: Int) {
        switch statusCode {
        case 200:
            loader?.stopLoader()
            alertController.showAlert(title: output.message,
                                      viewController: navigationController ?? self)
        case 500:
            loader?.stopLoader()
            self.showAlert(message: output.message)
        default:
            break
        }
        
    }
}

extension ResetPasswordViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        constraintLayout.constant = 120
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        constraintLayout.constant = 1
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        switch textField.tag {
        case 1:
            textField.removeError(newpassContainerView)
            showhideButton1.isHidden = false
        case 2:
            textField.removeError(retypepassContainerView)
            showhideButton2.isHidden = false
        case 3:
            textField.removeError(resetcodeContainerView)
            showhideButton3.isHidden = false
        default:
            break
        }
        return true
    }
}
