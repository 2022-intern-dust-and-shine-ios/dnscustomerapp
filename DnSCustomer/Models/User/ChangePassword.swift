//
//  ChangePassword.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 3/8/22.
//

import Foundation


struct InputChangePassword: Codable {
    
    var oldPassword: String
    var password: String
    var confirmPassword: String
    
    enum CodingKeys: String, CodingKey {
        case password
        case oldPassword = "old_password"
        case confirmPassword = "confirm_password"
    }
}

struct OutputChangePassword: Codable {
    var message: String
}
