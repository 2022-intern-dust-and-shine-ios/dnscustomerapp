//
//  RegisterPresenter.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 1/31/22.
//

import Foundation

protocol RegisterPresenterDelegate: AnyObject {
    func presentOutputable(output: OutputRegister, _ statusCode: Int)
    func presentError(_ error: String, _ statusCode: Int)
}

class RegisterPresenter: NSObject {
    
    private weak var delegate: RegisterPresenterDelegate?
    private let authService = AuthService()
    
    init(delegate: RegisterPresenterDelegate) {
        self.delegate = delegate
    }
    
    public func setRegister(input: InputRegister) {
        
        authService.register(input: input)
        { [weak self] output, statusCode, error  in
            guard let self = self else { return }
            guard let outputRegister = output, error == nil else {
                print(String(describing: error)
                )
                self.delegate?.presentError(error!.localizedDescription, statusCode ?? 0)
                return }
            self.delegate?.presentOutputable(output: outputRegister, statusCode ?? 0)
        }
    }
}
