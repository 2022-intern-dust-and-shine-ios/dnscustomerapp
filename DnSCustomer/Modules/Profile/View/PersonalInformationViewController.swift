//
//  ProfileViewController.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 1/14/22.
//

import UIKit

class PersonalInformationViewController: UIViewController {
    
    enum Constant {
        static let identifier: String = "ChangePasswordViewController"
    }
    
    //MARK: - IBOUTLETS
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var emailaddressTextField: UITextField!
    @IBOutlet weak var mobilenumberTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var housenoTextField: UITextField!
    @IBOutlet weak var streetTextField: UITextField!
    @IBOutlet weak var barangayTextField: UITextField!
    @IBOutlet weak var citymunicipalityTextField: UITextField!
    @IBOutlet weak var provinceTextField: UITextField!
    @IBOutlet weak var zipcodeTextField: UITextField!

    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    
    @IBOutlet weak var firstnameContainerView: UIView!
    @IBOutlet weak var lastnameContainerView: UIView!
    @IBOutlet weak var emailaddressContainerView: UIView!
    @IBOutlet weak var mobilenumberContainerView: UIView!
    @IBOutlet weak var passwordContainerView: UIView!
    @IBOutlet weak var housenoContainerView: UIView!
    @IBOutlet weak var streetContainerView: UIView!
    @IBOutlet weak var barangayContainerView: UIView!
    @IBOutlet weak var citymunicipalityContainerView: UIView!
    @IBOutlet weak var provinceContainerView: UIView!
    @IBOutlet weak var zipcodeContainerView: UIView!
    @IBOutlet weak var modalContainerView: UIView! {
        didSet {
            modalContainerView.setCornerRadius(6)
        }
    }
    
    //MARK: - PRESENTER
    private lazy var presenter = ProfilePresenter(delegate: self)
    
    //MARK: - SERVICE
    private lazy var keyboardService = KeyboardService(delegate: self)
    private lazy var toolbarService = ToolbarService(delegate: self)
    private var loader: IVActivityIndicator?
    
    let userDefault = UserDefaults.standard
    private var id: Int?
    private var isEdit: Bool = false {
        didSet {
            if isEdit {
                saveButton.isEnabled = true
                saveButton.backgroundColor = AppColor.blueColor
            } else {
                saveButton.isEnabled = false
                saveButton.backgroundColor = .gray
            }
        }
    }
    
    var firstname: String?
    var lastname: String?
    var email: String?
    var number: String?
    
    var houseno: String?
    var street: String?
    var barangay: String?
    var city: String?
    var province: String?
    var zipcode: String?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.backButtonTitle = ""
        self.navigationItem.setTitle("Manage Account")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loader = IVActivityIndicator(superView: self)
        loader?.showLoader(navController: self.navigationController ?? self)
        
        initViews()
        setupTextfields()
  
        setStateForTFs(isEnable: false)
        
        presenter.getUserData(token: userDefault.getToken()!)
        
        keyboardService.setupRegisterKbNotification()
        toolbarService.setupToolbar(textfields: [housenoTextField,
                                                zipcodeTextField,
                                                mobilenumberTextField])
    }
    
    private func initViews() {
    
        let radBtn: CGFloat = 4.0
        let val = CGFloat(10.0)
        let views = [firstnameContainerView, lastnameContainerView, emailaddressContainerView,
                     mobilenumberContainerView,passwordContainerView,
                     housenoContainerView, streetContainerView,
                     barangayContainerView,citymunicipalityContainerView,
                     provinceContainerView, zipcodeContainerView]
        views.forEach{ $0?.setCornerRadius(val)}
        passwordTextField.textColor = .gray
        
        [editButton,
         saveButton].forEach {
            $0?.setCornerRadius(radBtn)
        }
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.didTappedView))
        self.view.addGestureRecognizer(tapGesture)
    }
    
    
    
    private func setupTextfields() {
        let tfs = [firstNameTextField, lastNameTextField, emailaddressTextField,
                   mobilenumberTextField,
                    housenoTextField, streetTextField, barangayTextField, citymunicipalityTextField,
                   provinceTextField, zipcodeTextField]
        tfs.forEach { $0?.delegate = self }
    }
 
    private func setStateForTFs(isEnable: Bool) {
        let tfs = [firstNameTextField, lastNameTextField, emailaddressTextField,
                   mobilenumberTextField,
                    housenoTextField, streetTextField, barangayTextField, citymunicipalityTextField,
                   provinceTextField, zipcodeTextField]
        tfs.forEach {
            $0?.isEnabled = isEnable
            $0?.isUserInteractionEnabled = isEnable
            $0?.textColor = isEnable ? UIColor.black : .gray
        }
    }

    @objc func didTappedView() {
        self.view.endEditing(true)
        heightConstraint.constant = 1
    }
    
    
    private func showModalChangePassword() {
        guard let viewController = storyboard?.instantiateViewController(withIdentifier: Constant.identifier) as? ChangePasswordViewController else { return }
        viewController.modalPresentationStyle = .overCurrentContext
        present(viewController, animated: false, completion: nil)
    }
    
    @IBAction func changepassAction(_ sender: UIButton) {
        showModalChangePassword()
    }
    
    @IBAction func editAction(_ sender: UIButton) {
        sender.haptic()
        
        if isEdit {
            editButton.setTitle("EDIT", for: .normal)
            setStateForTFs(isEnable: false)
            isEdit = false
            defaultValues()
        } else {
            editButton.setTitle("CANCEL", for: .normal)
            setStateForTFs(isEnable: true)
            isEdit = true
        }
        
        func defaultValues() {
            firstNameTextField.text = firstname
            lastNameTextField.text = lastname
            emailaddressTextField.text = email
            
            housenoTextField.text = houseno
            streetTextField.text = street
            barangayTextField.text = barangay
            citymunicipalityTextField.text = city
            provinceTextField.text = province
            zipcodeTextField.text = zipcode
        }
    }
    
    func saveUserInfo() {
        setStateForTFs(isEnable: false)
        editButton.setTitle("EDIT", for: .normal)
        isEdit = false
        let firstname = firstNameTextField.text!
        let lastname = lastNameTextField.text!
        let mobilenumber = mobilenumberTextField.text!
        let email = emailaddressTextField.text!
        let houseno = housenoTextField.text!
        let street = streetTextField.text!
        let barangay = barangayTextField.text!
        let municipality = citymunicipalityTextField.text!
        let province = provinceTextField.text!
        let zipcode = zipcodeTextField.text!
        
        let user: InputUser = .init(firstName: firstname, lastName: lastname,
                                        mobileNumber: mobilenumber, email: email,
                                    houseNumber: Int(houseno)!, street: street,
                                        barangay: barangay, municipality: municipality,
                                        province: province, zipcode: zipcode)
        presenter.updateUser(id: id ?? 0, token: userDefault.getToken()!,
                             input: user)
    }
    
    @IBAction func saveAction(_ sender: UIButton) {
        sender.haptic()
        
        if isEdit {
            loader?.showLoader(navController: self.navigationController ?? self)
            saveUserInfo()
        }
    }
}

extension PersonalInformationViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        heightConstraint.constant = 1
        return true
    }
}

extension PersonalInformationViewController: ProfilePresenterDelegate {
    func showError(statusCode: Int) {
        self.showAlertWithCompletion(message: "Unable to process your request right now, Please try again later") {
            self.loader?.stopLoader()
        }
    }
    
    func showUserData(_ user: OutputUser) {
        print(user)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.loader?.stopLoader()
            if let user_data = user.data.first {
                
                self.id = user_data.id
                self.firstNameTextField.text = user_data.firstName
                self.lastNameTextField.text = user_data.lastName
                self.emailaddressTextField.text = user_data.email
                self.mobilenumberTextField.text = user_data.mobileNumber
                self.passwordTextField.text = self.userDefault.getPassword()
                
                self.firstname = user_data.firstName
                self.lastname = user_data.lastName
                self.email = user_data.email
                self.number = user_data.mobileNumber
                
            }
            
            if let user_data = user.data.first ,
                let address = user_data.address {
                
                self.housenoTextField.text = "\(address.houseNumber)"
                self.streetTextField.text = address.street
                self.barangayTextField.text = address.barangay
                self.citymunicipalityTextField.text = address.municipality
                self.provinceTextField.text = address.province
                self.zipcodeTextField.text = address.zipcode
                
                self.houseno = address.houseNumber
                self.street = address.street
                self.barangay = address.barangay
                self.city = address.municipality
                self.province = address.province
                self.zipcode = address.zipcode
            }
        }
    }
}

extension PersonalInformationViewController: KeyboardProtocol {
    func getKeyboardSize(size: CGSize) {
        heightConstraint.constant = size.height
    }
}

extension PersonalInformationViewController: ToolbarProtocol {
    func dismissKeyboard() {
        self.view.endEditing(true)
        heightConstraint.constant = 1
    }
}
