//
//  IVAlertController.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 1/17/22.
//

import Foundation
import UIKit


class IVAlertController: NSObject {
   
    private var backgroundView: UIView = {
        let view = UIView()
        view.backgroundColor = .black
        view.alpha = 0.0
        return view
    }()
    private var alertView: UIView = {
       let view = UIView()
        view.backgroundColor = .white
        view.clipsToBounds = true
        view.layer.cornerRadius = 12
        return view
    }()
  
    var dismiss: (() -> Void)?
    
    func showAlert(title: String, viewController: UIViewController?) {
    
        guard let targetView = viewController!.view else { return }
        backgroundView.frame = targetView.bounds
        
        //targetView.addSubview(backgroundView)
        viewController?.view.addSubview(backgroundView)
        viewController?.view.addSubview(alertView)
        
        alertView.frame = .init(x: 40, y: -300, width: targetView.frame.size.width - 80, height: 300)
        
        
        let img = UIImageView(image: UIImage(named: "success"))
        img.frame = .init(x: (alertView.frame.size.width / 2) - 60, y: 50, width: 120, height: 120)
        img.contentMode = .scaleAspectFit
        alertView.addSubview(img)
        let titleLabel = UILabel()
        titleLabel.frame = .init(x: 15, y: 160, width: alertView.frame.size.width - 30, height: 100)
        titleLabel.text = title
        titleLabel.numberOfLines = 3
        titleLabel.font = UIFont(name: "Montserrat-Medium", size: 16)
        titleLabel.textAlignment = .center
        alertView.addSubview(titleLabel)
        
        let okayButton = UIButton()
        okayButton.setTitle("OK", for: .normal)
        okayButton.titleLabel?.font = UIFont(name: "Montserrat-Medium", size: 16)
        okayButton.setTitleColor(AppColor.blueColor, for: .normal)
        okayButton.frame = .init(x: 0, y: alertView.frame.size.height - 50, width: alertView.frame.size.width, height: 50)
        okayButton.addTarget(self, action: #selector(dismissAlert), for: .touchUpInside)
        alertView.addSubview(okayButton)
        
        UIView.animate(withDuration: 0.25) {
            self.backgroundView.alpha = 0.6
        } completion: { done in
            if done {
                self.alertView.center = targetView.center
            }
        }

    }
    @objc func dismissAlert() {
        dismiss?()
        backgroundView.removeFromSuperview()
        alertView.removeFromSuperview()
    }
}
