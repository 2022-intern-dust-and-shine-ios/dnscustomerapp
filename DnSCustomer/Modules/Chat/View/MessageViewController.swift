//
//  MessageViewController.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 1/17/22.
//

import UIKit

class MessageViewController: UIViewController {

    //MARK: - OUTLETS
    @IBOutlet weak var messageTableView: UITableView!
    @IBOutlet weak var tfContainerView: UIView!
    @IBOutlet weak var baseContainerView: UIView!
    @IBOutlet weak var constraintLayout: NSLayoutConstraint!
    @IBOutlet weak var messageTextfield: UITextField!
    
    private var userPhoto: UIImageView = {
       let img = UIImageView(image: UIImage(named: "user"))
        img.contentMode = .scaleAspectFill
        img.clipsToBounds = true
        img.translatesAutoresizingMaskIntoConstraints = false
        img.widthAnchor.constraint(equalToConstant: 44).isActive = true
        img.setCornerRadius(22)
        return img
    }()
    private var nameLabel: UILabel = {
        let nameLabel = UILabel()
        nameLabel.text = "Jake Sison"
        nameLabel.textAlignment = .left
        nameLabel.font = UIFont(name: "Quicksand-SemiBold", size: 15)
        return nameLabel
    }()
    
    private var statusLabel: UILabel = {
        let statusLabel = UILabel()
        statusLabel.text = "Online"
        statusLabel.font = UIFont(name: "Quicksand-Regular", size: 12)
        statusLabel.textAlignment = .left
        return statusLabel
    }()
    
    //MARK: - SERVICE
    private lazy var keyboardService = KeyboardService(delegate: self)
    
    //MARK: - LifeCycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.setSyle(.white)
        self.navigationItem.leftItemsSupplementBackButton = true
        let stack = UIStackView(arrangedSubviews: [nameLabel, statusLabel])
        stack.axis = .vertical
        self.navigationItem.leftBarButtonItems = [UIBarButtonItem(customView: userPhoto),
                                                   UIBarButtonItem(customView: stack)]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initViews()
        configureTableViewandTextField()
        
        keyboardService.setupRegisterKbNotification()
    }
    
    private func initViews() {
        tfContainerView.layer.borderColor = AppColor.shadowColor.cgColor
        tfContainerView.layer.borderWidth = 0.5
        tfContainerView.setCornerRadius(4)
        baseContainerView.addShadowNoCorner()
    }
    
    private func configureTableViewandTextField() {
        messageTableView.delegate = self
        messageTableView.dataSource = self
        messageTableView.separatorStyle = .none
        
        messageTextfield.delegate = self
    }
    
    private func resignTextfield() {
        messageTextfield.resignFirstResponder()
        constraintLayout.constant = 70
    }
    
    @IBAction func sendAction(_ sender: UIButton) {
        resignTextfield()
    }
}

extension MessageViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let message = messages[indexPath.row]
        if message.mine {
            if let cell = tableView.dequeueReusableCell(withIdentifier: MyMessageTableViewCell.identifier, for: indexPath) as? MyMessageTableViewCell {
                cell.bind(message)
                return cell
            }
        } else {
            if let cell = tableView.dequeueReusableCell(withIdentifier: OtherMessageTableViewCell.identifier, for: indexPath) as? OtherMessageTableViewCell {
                cell.bind(message)
                return cell
            }
        }
  
         return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension MessageViewController: UITextFieldDelegate {

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        resignTextfield()
        return true
    }
}

extension MessageViewController: KeyboardProtocol {
    func getKeyboardSize(size: CGSize) {
        constraintLayout.constant = size.height + 70
    }
}

struct Mes {
     
    let message: String
    let mine: Bool
     
}

let messages:[Mes] = [
    Mes(message: "Hey there what's up!", mine: false),
    Mes(message: "How's you doin'", mine: true),
    Mes(message: "Im doin good right actually im planting kamote men", mine: false),
    Mes(message: "Really, come on men you should be at the city right know", mine: true),
    Mes(message: "I think i have to accompany you to the store and let buy some food", mine: false),
]
