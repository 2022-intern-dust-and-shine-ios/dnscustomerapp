//
//  CheckoutViewController.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 1/21/22.
//

import UIKit
import SDWebImage

enum PaymentMethod: String {
    case cash = "Cash"
    case card = "Credit/Debit Card"
}

class CheckoutViewController: UIViewController {
    
    //MARK: - OUTLETS
    @IBOutlet weak var containerView: UIView! {
        didSet {
            containerView.addShadow()
        }
    }
    @IBOutlet weak var companyImageView: UIImageView!
    @IBOutlet weak var datetimeLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var customernameLabel: UILabel!
    @IBOutlet weak var customerlocationLabel: UILabel!
    @IBOutlet weak var customercontactLabel: UILabel!
    @IBOutlet weak var noteLabel: UILabel!
    @IBOutlet weak var paymentLabel: UILabel!
    @IBOutlet weak var companynameLabel: UILabel!
    @IBOutlet weak var serviceLabel: UILabel!
    //MARK: - Presenter
    private lazy var presenter = CheckoutPresenter(delegate: self)
    
    private var alertController = IVAlertController()
   
    private let userDefault = UserDefaults.standard
    
    private var address: String?
    private var paymentMethod: PaymentMethod = .cash
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.setSyle(.transparent)
        self.navigationItem.backButtonTitle = ""
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        alertController.dismiss = { [weak self] in
            guard let self = self else { return }
            self.redirectToDashboard()
        }
        presenter.getCustomerDetails(token: userDefault.getToken()!)
    }
    
    private func setupCompanyImage() {
        let urlImage = URL(string: "https://lpn.boomtech.co/\(userDefault.getCompanyImage())")
        companyImageView.sd_setImage(with: urlImage, placeholderImage: UIImage())
        companyImageView.sd_imageTransition = .fade(duration: 1.0)
    }
    
    @IBAction func changePaymentAction(_ sender: UIButton) {
        showSheet()
    }
    
    @IBAction func checkoutAction(_ sender: UIButton) {
        sender.haptic()
        
        let total = userDefault.getTotal()
        let services = userDefault.getServices()
        let datetime = userDefault.getDateTime()
        let id = userDefault.getCompanyId()
        let note = userDefault.getNote()
        let maddress = address ?? "N/A"
        print("TOTAL \(total)")
        print("SERVICES \(services)")
        print("DATE TIME \(datetime)")
        print("ID \(id)")
        print("ADDRESS \(maddress)")
     
        switch paymentMethod {
        case .cash:
            presenter.postBooking(.init(companyId: id, address: maddress,
                                        schedDatetime: datetime, total: total,
                                        services: services, note: note),
                                        userDefault.getToken() ?? "")
        case .card:
            self.showAlert(message: "Unable to process your request, Please try another payment method")
        }
    }
    
    private func setDetails(user: OutputUser) {
        
        let services = userDefault.getListService()
        var servicesText = ""
        
        if let muser = user.data.first, let maddress = muser.address {
            address = "\(maddress.houseNumber) \(maddress.street) \(maddress.barangay), \(maddress.municipality), \(maddress.province)"
            customernameLabel.text = "\(muser.firstName) \(muser.lastName)"
            customercontactLabel.text = muser.mobileNumber
        } else {
            self.showAlertWithCompletion(message: "Unable to process your request, Check your Address details") {
                self.navigationController?.popViewController(animated: true)
            }
        }
     
        customerlocationLabel.text = address ?? "N/A"
        companynameLabel.text = userDefault.getCompanyName()
        datetimeLabel.text = userDefault.getDateTime()
        totalLabel.text = "₱\(userDefault.getTotal())"
        noteLabel.text = userDefault.getNote() ?? ""
        for service in services {
            servicesText+="\(service), "
        }
        if servicesText.count > 2 {
            servicesText.removeLast(2)
        }
        serviceLabel.text = servicesText
    }
 
    private func redirectToDashboard() {
        guard let viewController = Modules.Main.initialViewController as? UITabBarController else {
            return
        }
        viewController.modalPresentationStyle = .fullScreen
        self.present(viewController, animated: true, completion: nil)
    }
}

extension CheckoutViewController {
    private func showSheet() {
        let sheet = UIAlertController(title: "Payment Method", message: "", preferredStyle: .actionSheet)
        let cash = UIAlertAction(title: "Cash", style: .default) { _ in
            self.paymentLabel.text = PaymentMethod.cash.rawValue
            self.paymentMethod = .cash
        }
        let bank = UIAlertAction(title: "Debit/Credit Card", style: .default) { _ in
            self.paymentLabel.text = PaymentMethod.card.rawValue
            self.paymentMethod = .card
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        sheet.addAction(cash)
        sheet.addAction(bank)
        sheet.addAction(cancel)
        present(sheet, animated: true, completion: nil)
    }
}

extension CheckoutViewController: CheckoutPresenterDelegate {

    func showError(statusCode: Int) {
        switch statusCode {
        case 404:
            self.showAlert(message: "Sorry this company is Fully book")
        default:
            self.showAlert(message: "Something went wrong. Please try again")
        }
    }
    
    func showUserDetails(user: OutputUser) {
        setDetails(user: user)
    }
    
    func setBooking(booking: OutputCheckout) {
        print(booking)
        alertController.showAlert(title: "Thank You! You have successfully Booked!",
                                  viewController: self.navigationController ?? self)
    }
}

//      guard let viewController = storyboard?.instantiateViewController(withIdentifier: TermsAndConditionViewController.Constant.id) as?
//                TermsAndConditionViewController else { return }
//        viewController.modalPresentationStyle = .overCurrentContext
//        viewController.checkoutViewController = self
//        present(viewController, animated: false, completion: nil)
//        navigationController?.pushViewController(viewController, animated: true)

