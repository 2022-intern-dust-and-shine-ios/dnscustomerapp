//
//  CompanyServiceViewController.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 1/27/22.
//

import UIKit

class CompanyServiceViewController: UIViewController {
    
    //MARK: - OUTLETS
    @IBOutlet weak var containerView: UIView! {
        didSet {
            containerView.addShadow()
        }
    }
    @IBOutlet weak var plusButton: UIButton!
    @IBOutlet weak var minusButton: UIButton!
    @IBOutlet weak var serviceLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var checkButton: UIButton!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel! {
        didSet {
            countLabel.setCornerRadius(8)
        }
    }
   
    //MARK: - PROPERTIES
    var count: Int = 0 {
        didSet {
            countLabel.text = "\(count)"
        }
    }
    
    var index: Int?
    var key: Int?
    var service: MService?
    var isCheck: Bool = true
    var total: Double = 0
    
    //MARK: - CLOSURES
    var selectedService: ((MService, Bool, Int,Int,Double) -> ())?
    var plusTotal: ((Double) -> ())?
    var minusTotal: ((Double) -> ())?
    var flagService: ((Bool) -> ())?
   
    //MARK: - APP LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
     
    }

    public func bind(_ service: MService, _ index: Int, _ key: Int) {
        
        self.key = key
        self.index = index
        self.service = service
        serviceLabel.text = service.name
        descriptionLabel.text = service.description
        total = service.price
        timeLabel.text = "₱\(service.price) / \(service.time) \(String(service.time) == "1" ? "hour":"hours")"
    }

    @IBAction func minusAction(_ sender: UIButton) {
        if count > 0 && isCheck {
            count-=1
            minusTotal?(service?.price ?? 0)
            total-=service?.price ?? 0
        }
    }
    
    @IBAction func plusAction(_ sender: UIButton) {
        if isCheck {
            count+=1
            plusTotal?(service?.price ?? 0)
            total+=service?.price ?? 0
        }
    }
    
    @IBAction func checkAction(_ sender: UIButton) {
        let uncheckImage = UIImage(named: "uncheck")!
        let checkImage = UIImage(named: "check")!
        
        guard count != 0 else { return }
        
        if sender.isSelected {
            sender.isSelected = false
        } else {
            sender.isSelected = true
            
        }
        isCheck = sender.isSelected
        let price = Int(self.service?.price ?? 0) * Int(count)
        selectedService?(service!, isCheck, index ?? 0, key ?? 0, Double(price))
        print("COUNT \(self.count) PRICE \(price) COUNT DBL \(Double(count))")
        if isCheck == false {
            count = 0
            plusButton.enabled()
            minusButton.enabled()
            isCheck = !isCheck
        } else {
            isCheck = !isCheck
            plusButton.disabled()
            minusButton.disabled()
        }
        
        checkButton.setImage(sender.isSelected ? checkImage:uncheckImage, for: .normal)
    }
}

extension CompanyServiceViewController: UITextFieldDelegate {
  
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
