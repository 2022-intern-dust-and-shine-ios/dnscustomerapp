//
//  DashboardViewController.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 1/17/22.
//

import UIKit

class DashboardViewController: UIViewController {
    
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var searchContainerView: UIView! {
        didSet {
           
            searchContainerView.layer.borderWidth = 1.0
            searchContainerView.layer.borderColor = AppColor.blueColor.cgColor
            searchContainerView.setCornerRadius(4)
        }
    }
    @IBOutlet weak var bannerContainerView: UIView! {
        didSet {
            bannerContainerView.setCornerRadius(CGFloat(8))
        }
    }
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var recommendationCollectionView: UICollectionView!
   
    
    fileprivate var recommendations : [RecommendedCompany] = [] {
        didSet {
            recommendationCollectionView.reloadData()
        }
    }
   
    private lazy var presenter = DashboardPresenter(delegate: self)
    private let userDefault = UserDefaults.standard
    
    var navBarController: NavBarViewController?
    var featuredController: FeaturedServiceViewController?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.setSyle(.hidden)
        self.navigationItem.backButtonTitle = ""
    }
   
    override func viewDidLoad() {
        super.viewDidLoad()
        searchTextField.delegate = self
        configureCollectionView()
        
        if Connectivity.isConnectedToInternet() {
            presenter.getRecommendedCompanies(token: userDefault.getToken()!)
        } else {
            self.showAlert(actionTitle: "Okay", message: "Please check your internet connection")
        }
    
        setupNavBarActions()

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        searchTextField.endEditing(true)
        searchTextField.resignFirstResponder()
    }
    
    private func configureCollectionView() {
        recommendationCollectionView.delegate = self
        recommendationCollectionView.dataSource = self
    }

    private func setupNavBarActions() {
        navBarController?.showPersonalInfo = { [weak self] in
            guard let self = self else { return }
            guard let viewController = Modules.Profile.initialViewController as? ProfileViewController else { return }
            viewController.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(viewController, animated: true)
        }
     
        navBarController?.showNotification = { [weak self] in
            guard let self = self else { return }
            guard let viewController = Modules.Notification.initialViewController as? NotificationViewController else { return }
            viewController.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    @IBAction func viewallAction(_ sender: UIButton) {
        redirectSearch(.all)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
      
        if segue.identifier == "Segue1" {
            guard let navBarViewController = segue.destination as? NavBarViewController else {
                return
            }

            self.navBarController = navBarViewController
            
        } else if segue.identifier == "Segue2" {
            guard let vc = segue.destination as? FeaturedServiceViewController else {
                return
            }
            self.featuredController = vc
            vc.height = {
                [weak self] height in
                guard let self  = self else { return }
                self.heightConstraint.constant = height + 16
                self.view.layoutIfNeeded()
                print(self.heightConstraint.constant)
            }
        }
    }
}

extension DashboardViewController: UICollectionViewDelegate,
                                        UICollectionViewDataSource,
                                        UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return recommendations.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: RecommendationCVC.identifier, for: indexPath) as? RecommendationCVC {
            cell.bind(recommendations[indexPath.row])
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 16, bottom: 10, right: 16)
    }
    
 
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let recommendation = recommendations[indexPath.row]
        userDefault.setCompanyName(value: recommendation.company.name)
        userDefault.setCompanyId(value: recommendation.companyId)
        userDefault.setCompanyRating(value: recommendation.rating)
        guard let viewController = Modules.Detail.initialViewController as? CompanyDetailViewController else { return }
        viewController.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

extension DashboardViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        redirectSearch(.filter)
    }

    func redirectSearch(_ type: SearchType) {
        guard let viewController = Modules.Search.initialViewController as? SearchViewController else { return }
        viewController.hidesBottomBarWhenPushed = true
        viewController.searchType = type
        self.navigationController?.pushViewController(viewController, animated: false)
    }
}

extension DashboardViewController: DashboardPresenterDelegate {
    func showRecommendedCompanies(_ companies: Company<[RecommendedCompany]>) {
        self.recommendations = companies.data
    }
    func showError(statusCode: Int) {
        self.showAlert(message: "Unable to process your request right now, Please log in again")
    }
}


