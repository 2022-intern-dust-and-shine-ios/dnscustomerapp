//
//  VerifyEmailViewController.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 1/18/22.
//

import UIKit

class VerifyEmailViewController: UIViewController {

    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchContainerView: UIView!
    @IBOutlet weak var mainContainerView: UIView!
    @IBOutlet weak var searchButton: UIButton!
    
    //MARK: - PRESENTER
    private lazy var presenter = ForgotPasswordPresenter(delegate: self)
    
    private var loader: IVActivityIndicator?
    
    var signinviewController: UIViewController?
    var email: String?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.setTitle("Forgot Password")
        self.navigationItem.backButtonTitle = ""
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loader = IVActivityIndicator(superView: self)
        
        initViews()
        searchTextField.delegate = self
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        searchTextField.resignFirstResponder()
        //vc.setupRegisterKbNotification()
        dismiss(animated: false, completion: nil)
    }
    
    private func initViews() {
        [searchContainerView, mainContainerView].forEach {
            $0?.layer.borderWidth = 0.5
            $0?.layer.borderColor = UIColor.darkGray.cgColor
            $0?.setCornerRadius(6)
            $0?.lightShadow()
        }
        mainContainerView.layer.borderColor = UIColor.darkGray.cgColor
        searchButton.setCornerRadius(6)
    }
    private func redirectResetPass( _ email: String) {
        guard let viewController = storyboard?.instantiateViewController(withIdentifier: "ResetPasswordViewController") as? ResetPasswordViewController else {
            return
        }
     
        viewController.email = email
        signinviewController?.navigationController?.pushViewController(viewController,
                                                                       animated: true)
    }
    
    @IBAction func searchAction(_ sender: UIButton) {
   
        let search = searchTextField.text
        if search!.isNotEmpty() {
            self.email = search ?? ""
            loader?.showLoader(navController: navigationController ?? self)
            presenter.postForgotPassword(input: .init(email: search ?? ""))
            return
        }
        self.showAlert(message: "Invalid Email")
    }
}

extension VerifyEmailViewController: ForgotPasswordPresenterDelegate {
    func forgotPassword(output: OutputForgotPassword, statusCode: Int) {
        print(output.status ?? "NO OUTPUT")
        switch statusCode {
        case 200:
            self.loader?.stopLoader()
            self.showAlertWithCompletion(title: "Reset Password",
                                         actionTitle: "Okay",
                                         message: "\(output.status ?? "")") {
  
                self.dismiss(animated: false) {
                    self.redirectResetPass(self.email ?? "")
                }
            }
      
        case 401:
            loader?.stopLoader()
            self.showAlert(message: output.message ?? "")
        default:
            break
        }
    }
    
    func showError() {
        self.showAlert(message: "Something went wrong")
    }
}

extension VerifyEmailViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
