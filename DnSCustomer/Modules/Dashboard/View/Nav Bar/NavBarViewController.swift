//
//  NavBarViewController.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 1/17/22.
//

import UIKit



class NavBarViewController: UIViewController {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var userImageView: UIImageView! {
        didSet {
            userImageView.addShadow()
            userImageView.clipsToBounds = true
            userImageView.isUserInteractionEnabled = true
        }
    }
    @IBOutlet weak var containerView: UIView! {
        didSet {
            containerView.layer.borderColor = AppColor.blueColor.cgColor
            containerView.layer.borderWidth = 1.0
            containerView.setCornerRadius(CGFloat(8))
        }
    }
 
    //MARK: - Presenter
    private lazy var presenter = ProfilePresenter(delegate: self)
    
    private let userDefault = UserDefaults.standard
    var showPersonalInfo: (() -> Void)?
    var showNotification: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.getUserData(token: userDefault.getToken()!)
        setUpTapGesture()
    }
    
    private func setUpTapGesture() {
        let tapGesture = UITapGestureRecognizer(target: self,
                                                action: #selector(self.didTappedProfile))
        userImageView.addGestureRecognizer(tapGesture)
    }
    
    @objc func didTappedProfile() {
        redirectToProfile()
    }
    
    private func redirectToProfile() {
        guard let viewController = Modules.Profile.initialViewController as?
                ProfileViewController else { return }
        viewController.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func notificationAction(_ sender: UIButton) {
       showNotification?()
    }
}

extension NavBarViewController: ProfilePresenterDelegate {
    
    func showError(statusCode: Int) {
        
    }
    
    func showUserData(_ user: OutputUser) {
        let muser = user.data[0]
        let _ = muser.firstName
        let _ = muser.lastName
        if let address = muser.address {
            
            let houseno = address.houseNumber
            let _ = address.street
            let barangay = address.barangay
            let municipality = address.municipality
            let province = address.province
            let completeAddress = "#\(houseno) \(barangay) \(municipality) \(province)"
            addressLabel.text = completeAddress
            
            let fullname = "\(barangay) \(municipality)"
            nameLabel.text = fullname
        }
    }
}
