//
//  BookService.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 2/18/22.
//

import Foundation
import RealmSwift


struct OutputBookService: Codable {
    var data: CDATAs
    var message: String
}

typealias CDATAs = [CData]

struct CData: Codable {
    var id: Int
    var userId: Int
    var companyId: Int
    var schedDateTime: String
    var startDateTime: String?
    var endDateTime: String?
    var address: String
    var latitude: Double?
    var longitude: Double?
    var status: Int
    var total: Int
    var paymentStatus: Int
    var createdAt: String
    var updatedAt: String
    var services: WServices
    var workers: Workers
    var customer: User
    var note: String?
    
    enum CodingKeys: String, CodingKey {
        case id, address, latitude, longitude, status, total
        case services, workers, customer, note
        case userId = "user_id"
        case companyId = "company_id"
        case schedDateTime = "sched_datetime"
        case startDateTime = "start_datetime"
        case endDateTime = "end_datetime"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case paymentStatus = "payment_status"
    }
}

