//
//  UINavigationItem+TitleStyle.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 1/14/22.
//

import Foundation
import UIKit




extension UINavigationItem {
    public func setTitle(_ title: String) {
        let width: CGFloat = 200
        let height: CGFloat = 40
        
        let mview = UIView()
        let titleLabel = UILabel()
        
        mview.frame = CGRect(x: 0, y: 0, width: width, height: height)
        titleLabel.frame = CGRect(x: 0, y: 0, width: width, height: height)
        
        titleLabel.text = title
        titleLabel.font = UIFont(name: "Montserrat-SemiBold", size: 18)
        titleLabel.textAlignment = .center
     
        titleLabel.textColor = AppColor.yellowColor
        mview.addSubview(titleLabel)
        self.titleView = mview
    }
}
