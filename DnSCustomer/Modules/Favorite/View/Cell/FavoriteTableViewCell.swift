//
//  FavoriteTableViewCell.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 1/17/22.
//

import UIKit

class FavoriteTableViewCell: UITableViewCell {
    
    static let identifier: String = "FavoriteTableViewCell"
    
    @IBOutlet weak var containerView: UIView! {
        didSet {
            containerView.lightShadow()
            containerView.setCornerRadius(8)
        }
    }
    @IBOutlet weak var companyImageView: UIImageView! {
        didSet {
           
            companyImageView.setCornerRadius(8)
            companyImageView.clipsToBounds = true
        }
    }
    @IBOutlet weak var ratingContainerView: UIView! {
        didSet {
            ratingContainerView.setCornerOutline(4.0)
        }
    }
    @IBOutlet weak var companynameLabel: UILabel!
    @IBOutlet weak var ratings: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        ratingContainerView.isHidden = true
        companynameLabel.dropShadow()
        locationLabel.dropShadow()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
