//
//  CompanyService.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 2/10/22.
//

import Foundation
import Alamofire

typealias CompletionHandler<T:Codable> = ((T?, Int?, AFError?) -> Void)

struct CompanyService {
    
    public init() {}
    
    public func getServices(token: String, completion: @escaping CompletionHandler<MCompanyService>) {
        AF.request(CompanyAPI.services(token)).responseDecodable(of: MCompanyService.self)
        { response in
            let statusCode = response.response?.statusCode
            switch response.result {
            case .success(let service):
                completion(service, statusCode, nil)
            case .failure(let error):
                completion(nil, statusCode, error)
            }
        }
    }
    
    public func getCompanies(token: String, completion: @escaping CompletionHandler<Company<Companies>>) {
        AF.request(CompanyAPI.companies(token)).responseDecodable(of: Company<Companies>.self)
        { response in
            let statusCode = response.response?.statusCode
            switch response.result {
            case .success(let company):
                completion(company, statusCode, nil)
            case .failure(let error):
                completion(nil, statusCode, error)
            }
        }
    }
    public func getCompany(id: Int, token: String,
                           completion: @escaping CompletionHandler<Company<MCompany>>) {
        AF.request(CompanyAPI.getCompany(_token: token, id)).responseDecodable(of: Company<MCompany>.self) { response in
            let statusCode = response.response?.statusCode
            switch response.result {
            case .success(let company):
                completion(company, statusCode, nil)
            case .failure(let error):
                print("ERRRR \(String(describing: error))")
                completion(nil, statusCode, error)
            }
        }
        .cURLDescription { description in
            print(description)
        }
    }
    public func filterCompaniesByService(id: Int, token: String, completion: @escaping CompletionHandler<Company<Companies>>) {
        AF.request(CompanyAPI.filterCompaniesByService(token, id)).responseDecodable(of: Company<Companies>.self) { response in
            let statusCode = response.response?.statusCode
            switch response.result {
            case .success(let companies):
                completion(companies, statusCode, nil)
            case .failure(let error):
                completion(nil, statusCode, error)
            }
        }
    }
    public func searchCompanies(keyword: String, token: String,
                                completion: @escaping CompletionHandler<Company<Companies>>) {
        AF.request(CompanyAPI.search(keyword, token)).responseDecodable(of: Company<Companies>.self)
        { response in
            let statusCode = response.response?.statusCode
            switch response.result {
            case .success(let company):
                completion(company, statusCode, nil)
            case .failure(let error):
                print(error.localizedDescription)
                print(String(describing: error))
                completion(nil, statusCode, error)
            }
        }
    }
    public func getRecommendedCompanies(token: String, completion: @escaping CompletionHandler<Company<[RecommendedCompany]>>) {
        AF.request(CompanyAPI.recommendation(token)).responseDecodable(of: Company<[RecommendedCompany]>.self) { response in
            let statusCode = response.response?.statusCode
            switch response.result {
            case .success(let recommendedCompanies):
                completion(recommendedCompanies, statusCode, nil)
            case .failure(let error):
                completion(nil, statusCode, error)
            }
        }
    }
    

}

