//
//  ForgotPasswordPresenter.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 3/31/22.
//

import Foundation

protocol ForgotPasswordPresenterDelegate: AnyObject {
    func forgotPassword(output: OutputForgotPassword, statusCode: Int)
    func showError()
}

class ForgotPasswordPresenter: NSObject {
    
    private weak var delegate: ForgotPasswordPresenterDelegate?
    private let customerService = CustomerService()
    
    init(delegate: ForgotPasswordPresenterDelegate) {
        self.delegate = delegate
    }
    
    public func postForgotPassword(input: InputForgotPassword) {
        customerService.postForgotPassword(input: input) {
            [weak self] output, statusCode, error in
            guard let self = self else { return }
            guard let output = output, error == nil else {
                self.delegate?.showError()
                return }
            self.delegate?.forgotPassword(output: output, statusCode: statusCode ?? 0)
        }
    }
}
