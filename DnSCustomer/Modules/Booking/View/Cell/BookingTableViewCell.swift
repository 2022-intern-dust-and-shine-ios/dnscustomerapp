//
//  BookingTableViewCell.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 1/19/22.
//

import UIKit

class BookingTableViewCell: UITableViewCell {

    static let identifier: String = "BookingTableViewCell"
    
    //MARK: - IBOUTLETS
    @IBOutlet weak var nameContainerView: UIView! {
        didSet {
            let radius = nameContainerView.frame.size.width / 2
            nameContainerView.setCornerRadius(radius)
            nameContainerView.clipsToBounds = true
        }
    }
    @IBOutlet weak var firstletterLabel: UILabel!
    @IBOutlet weak var serviceLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var paymentLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        
        containerView.lightShadow()
        containerView.setCornerRadius(6)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    public func bind(_ data: CData, letter: String) {
        
        serviceLabel.text = services(data.services)
        dateLabel.text = data.schedDateTime
        totalLabel.text = "₱\(data.total)"
        firstletterLabel.text = letter
        paymentLabel.text = data.paymentStatus == 0 ? "Not yet paid" : "Payment Acknowledged"
    }
    private func services(_ services: WServices) -> String {
        var service_str = ""
        for service in services {
            service_str+="\(service.name), "
        }
        if service_str.count > 2 {
            service_str.removeLast(2)
        }
        return service_str
    }
}
