//
//  KeyboardProtocol.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 3/22/22.
//

import Foundation

import UIKit

protocol KeyboardProtocol: AnyObject {
    func getKeyboardSize(size: CGSize)
}

class KeyboardService: NSObject {
    
    private weak var delegate: KeyboardProtocol?
    
    init(delegate: KeyboardProtocol) {
        self.delegate = delegate
    }
    
    public func setupRegisterKbNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    
    @objc internal func keyboardWillShow(_ notification : Notification?) -> Void {
        
        var _kbSize:CGSize!
        
        if let info = notification?.userInfo {

            let frameEndUserInfoKey = UIResponder.keyboardFrameEndUserInfoKey
            
            //  Getting UIKeyboardSize.
            if let kbFrame = info[frameEndUserInfoKey] as? CGRect {
                
                let screenSize = UIScreen.main.bounds
                
                //Calculating actual keyboard displayed size, keyboard frame may be different when hardware keyboard is attached (Bug ID: #469) (Bug ID: #381)
                let intersectRect = kbFrame.intersection(screenSize)
                
                if intersectRect.isNull {
                    _kbSize = CGSize(width: screenSize.size.width, height: 0)
                } else {
                    _kbSize = intersectRect.size
                }
                delegate?.getKeyboardSize(size: _kbSize)
                
            }
        }
    }
}

