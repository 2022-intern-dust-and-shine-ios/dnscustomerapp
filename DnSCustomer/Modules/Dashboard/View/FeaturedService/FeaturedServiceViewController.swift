//
//  FeaturedServiceViewController.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 1/19/22.
//

import UIKit



class FeaturedServiceViewController: UIViewController {

    //MARK: - OUTLETS
    @IBOutlet weak var featuredCollectionView: UICollectionView!

    private var loader : UIActivityIndicatorView = {
        let loader = UIActivityIndicatorView()
        loader.style = .medium
        loader.translatesAutoresizingMaskIntoConstraints = false
        return loader
    }()
    
    //@IBOutlet weak var containerView: UIView!
    
    private lazy var presenter = FeaturedServicePresenter(delegate: self)
    
    private let userDefault = UserDefaults.standard
    
    private var items: MServices = [] {
        didSet {
            featuredCollectionView.reloadData()
            
            if items.count == 0 {
                
                loader.startAnimating()
                featuredCollectionView.backgroundView = loader
                loader.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
                loader.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
            } else {
                featuredCollectionView.backgroundView = nil
            }
        }
    }
    
    var height: ((CGFloat) -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureCollectionView()
        presenter.getFeaturedService(userDefault.getToken()!)
        let longTapGesture = UISwipeGestureRecognizer(target: self, action: #selector(self.didSwiped))
        self.view.addGestureRecognizer(longTapGesture)
        self.view.isUserInteractionEnabled = true
        
    }
    
    
    private func configureCollectionView() {
        featuredCollectionView.delegate = self
        featuredCollectionView.dataSource = self
        
        let layout = UICollectionViewFlowLayout()
        let width = (UIScreen.main.bounds.width / 2) - 15
        let height = (featuredCollectionView.frame.height / 2) - 8
        layout.itemSize = CGSize(width: width, height: height)
        featuredCollectionView.setCollectionViewLayout(layout, animated: true)
    }
 
    private func redirectToServiceCompany(id: Int) {
        guard let viewController = Modules.ServiceCompany.initialViewController as? ServiceCompanyViewController else {
             return
        }
        viewController.id = id
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @objc func didSwiped() {
      
        items = []
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.presenter.getFeaturedService(self.userDefault.getToken()!)
        }
    }
}

extension FeaturedServiceViewController: UICollectionViewDataSource,
                                         UICollectionViewDelegate,
                                         UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FeaturedServiceCVC.identidier, for: indexPath) as? FeaturedServiceCVC {
            let service = items[indexPath.row]
            cell.bind(service)
            return cell
        }
        return UICollectionViewCell()
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        userDefault.setCompanyName(value: items[indexPath.row].name)
        redirectToServiceCompany(id: items[indexPath.row].id)
    
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 1, left: 10, bottom: 1, right: 10)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
}

extension FeaturedServiceViewController: FeaturedServicePresenterDelegate {
   
    func showFeaturedServices(services: MCompanyService) {
        var default_height:Double = 180.0
        
        items = services.data
        let len = services.data.count
        let div = len / 2
        let double_len = len % 2 == 0
        
        if len != 1 {
            default_height = Double(double_len ? div : div + 1) * default_height
        }
      
        print(default_height)
        self.height?(CGFloat(default_height))
        loader.stopAnimating()
        
        
        if items.isEmpty {
            
        }
     }
    
    func showError(statusCode: Int) {
        loader.stopAnimating()
        self.showAlert(message: "Unable to process your request right now, Please log in again")
    }
}

extension FeaturedServiceViewController {
    public enum RoundingPrecision {
        case ones
        case tenths
        case hundredths
    }

    // Round to the specific decimal place
    public func preciseRound(
        _ value: Double,
        precision: RoundingPrecision = .ones) -> Double
    {
        switch precision {
        case .ones:
            return round(value)
        case .tenths:
            return round(value * 10) / 10.0
        case .hundredths:
            return round(value * 100) / 100.0
        }
    }
}
