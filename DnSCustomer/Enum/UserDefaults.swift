//
//  UserDefaults.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 1/31/22.
//

import Foundation


enum UserDefaultKeys {
    
    case token
    case company
    case companyname
    case company_image
    case address
    case datetime
    case services
    case total
    case listofservice
    case note
    case password
    case booking_id
    case f_letter
    case rating
    
    //TODO: - Refactor this to raw value and conform self to String
    var id: String {
        switch self {
        case .token:
            return "DnSCustomer.token"
        case .company:
            return "DnSCustomer.company"
        case .address:
            return "DnSCustomer.address"
        case .datetime:
            return "DnSCustomer.datetime"
        case .services:
            return "DnSCustomer.services"
        case .total:
            return "DnSCustomer.total"
        case .companyname:
            return "DnSCustomer.companyname"
        case .listofservice:
            return "DnSCustomer.listofservice"
        case .note:
            return "DnSCustomer.note"
        case .password:
            return "DnSCustomer.password"
        case .booking_id:
            return "DnSCustomer.booking_id"
        case .company_image:
            return "DnSCustomer.company_image"
        case .f_letter:
            return "DnSCustomer.f_letter"
        case .rating:
            return "DnSCustomer.rating"
        }
    }
}
