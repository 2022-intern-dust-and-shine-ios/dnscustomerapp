//
//  Connectivity.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 3/29/22.
//

import Foundation

import Alamofire
class Connectivity {
    class func isConnectedToInternet() -> Bool {
        return NetworkReachabilityManager()?.isReachable ?? false
    }
}
