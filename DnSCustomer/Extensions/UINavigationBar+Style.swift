//
//  UINavigationBar+Style.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 1/14/22.
//

import Foundation
import UIKit



extension UINavigationBar {
 
    public func setSyle(_ style: IVNavigatioNBarStyle) {
        self.setBarColor(style.barColor)
        self.tintColor = AppColor.blueColor
        isHidden = style.isHidden
    }
    func setBarColor(_ barColor: UIColor?) {
        if barColor != nil && barColor!.cgColor.alpha == 0 {
            // if transparent color then use transparent nav bar
            setBackgroundImage(UIImage(), for: .default)
            hideShadow(true)
        } else if barColor != nil {
            // use custom color
            
            setBackgroundImage(self.image(with: barColor!), for: .default)
            hideShadow(true)
        } else {
            // restore original nav bar color
            setBackgroundImage(nil, for: .default)
            hideShadow(false)
        }
    }
    
    private func hideShadow(_ doHide: Bool) {
        self.shadowImage = doHide ? UIImage() : nil
    }
    
    private func image(with color: UIColor) -> UIImage {
        let rect = CGRect(x: CGFloat(0.0), y: CGFloat(0.0), width: CGFloat(1.0), height: CGFloat(1.0))
        UIGraphicsBeginImageContext(rect.size)
        if let context = UIGraphicsGetCurrentContext() {
            context.setFillColor(color.cgColor)
            context.fill(rect)
        }
        let image: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}
