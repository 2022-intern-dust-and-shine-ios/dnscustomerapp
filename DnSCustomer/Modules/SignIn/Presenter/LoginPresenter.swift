//
//  LoginPresenter.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 1/31/22.
//

import Foundation


protocol LoginPresenterDelegate: AnyObject {
    func presentOutputable(output: OutputLogin, _ statusCode: Int)
    func presentError(_ error: String, _ statusCode: Int)
}

class LoginPresenter: NSObject {
    
    private weak var delegate: LoginPresenterDelegate?
    
    private let authService = AuthService()
    
    init(delegate: LoginPresenterDelegate) {
        self.delegate = delegate
    }
    
    public func setLogin(email: String, password: String) {
        authService.login(input: .init(email: email, password: password))
        { [weak self] output, statusCode, error in
            guard let self = self else { return }
            guard let outputLogin = output, error == nil else {
                self.delegate?.presentError(error!.localizedDescription, statusCode ?? 0)
                return }
            self.delegate?.presentOutputable(output: outputLogin, statusCode ?? 0)
        }
    }
}
