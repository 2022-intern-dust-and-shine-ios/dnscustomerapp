//
//  Modules.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 1/14/22.
//

import Foundation
import UIKit


public enum Modules: String {
    
    case Main
    case SignIn
    case SignUp
    case Profile
    case Forgot
    case Notification
    case Search
    case Detail
    case Checkout
    case Chat
    case ServiceCompany
    
    var storyboard: UIStoryboard {
        switch self {
        case .Main,
             .Notification,
             .Forgot,
             .SignIn,
             .SignUp,
             .Profile,
             .Detail,
             .Checkout,
             .Chat,
             .Search,
             .ServiceCompany:
            return UIStoryboard(name: self.rawValue, bundle: nil)
        }
    }
    var initialViewController: UIViewController {
        switch self {
        case .Main,
             .Notification,
             .Forgot,
             .SignIn,
             .SignUp,
             .Profile,
             .Detail,
             .Checkout,
             .Chat,
             .Search,
             .ServiceCompany:
            return storyboard.instantiateInitialViewController() ?? UIViewController()
        }
    }
}
