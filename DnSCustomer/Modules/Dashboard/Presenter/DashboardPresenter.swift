//
//  DashboardPresenter.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 1/24/22.
//

import Foundation

protocol DashboardPresenterDelegate: AnyObject {
    func showRecommendedCompanies(_ companies: Company<[RecommendedCompany]>)
    func showError(statusCode: Int)
}

class DashboardPresenter: NSObject {
    private let companyService = CompanyService()
    private weak var delegate: DashboardPresenterDelegate?
    init(delegate: DashboardPresenterDelegate) {
        self.delegate = delegate
    }
    public func getRecommendedCompanies(token: String) {
        companyService.getRecommendedCompanies(token: token) {
            [weak self] companies, statusCode, error in
            guard let self = self else { return }
            guard let companies = companies, error == nil else {
                print(String(describing: error))
                self.delegate?.showError(statusCode: statusCode ?? 0)
                return }
            self.delegate?.showRecommendedCompanies(companies)
        }
    }
}
