//
//  AppColor.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 1/14/22.
//

import Foundation
import UIKit


struct AppColor {
    
    public static var yellowColor: UIColor {
        return UIColor().colorWithHexString(hex: "#F5C711")
    }
    public static var blueColor: UIColor {
        return UIColor().colorWithHexString(hex: "#38B0C8")
    }
    public static var containerViewColor: UIColor {
        return UIColor().colorWithHexString(hex: "#F2F4F8")
    }
    public static var unselectedColor: UIColor {
        return UIColor().colorWithHexString(hex: "#9E9E9E")
    }
    public static var shadowColor: UIColor {
        return UIColor().colorWithHexString(hex: "#9E9E9E")
    }
}
