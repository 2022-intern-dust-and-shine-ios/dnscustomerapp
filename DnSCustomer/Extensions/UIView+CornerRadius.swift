//
//  UIView+CornerRadius.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 1/14/22.
//

import Foundation
import UIKit


extension UIView {
    
    func setCornerRadius(_ value: CGFloat) {
        self.layer.cornerRadius = value
    }
    func setCornerOutline(_ value: CGFloat, borderColor: UIColor? = AppColor.yellowColor) {
        self.layer.borderColor = borderColor?.cgColor
        self.layer.borderWidth = 1.0
        self.layer.cornerRadius = value
    }
}
