//
//  UITableView+EmptyState.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 4/5/22.
//

import Foundation
import UIKit

enum EmptyImageState {
    case company
    case service
    case notif
    case history
}

extension UITableView {
    
    func empty(_ state: EmptyImageState) {
        
        let img = UIImageView()
        let lbl = UILabel()
        
        switch state {
        case .company:
            img.image = UIImage(named: "no-company")
            lbl.text = "No companies are providing \n this service at the moment"
        case .service:
            img.image = UIImage(named: "no-service")
            lbl.text = "No transactions yet"
        case .notif:
            img.image = UIImage(named: "no-service")
            lbl.text = "No transactions yet"
        case .history:
            img.image = UIImage(named: "no-service")
            lbl.text = "No transactions yet"
        }
        
        
        img.contentMode = .scaleAspectFit
        img.translatesAutoresizingMaskIntoConstraints = false
        
     
        lbl.numberOfLines = 2
        lbl.textColor = .gray
        
        lbl.font = UIFont(name: "Montserrat-Medium", size: 16.0)
        lbl.frame = CGRect(origin: .zero, size: .init(width: 200, height: 80))
        lbl.translatesAutoresizingMaskIntoConstraints = false
        
        let chilStackView = UIStackView(arrangedSubviews: [img, lbl])
        chilStackView.axis = .vertical
        chilStackView.alignment = .center
        chilStackView.distribution = .equalSpacing
        chilStackView.spacing = 8
       
        let parentStackView = UIStackView(arrangedSubviews: [chilStackView])
        parentStackView.axis = .horizontal
        parentStackView.alignment = .center
     
        self.backgroundView = parentStackView
        
        img.widthAnchor.constraint(equalToConstant: 200).isActive = true
        img.heightAnchor.constraint(equalToConstant: 200).isActive = true
        
        chilStackView.widthAnchor.constraint(equalToConstant: 300).isActive = true
    }
}

extension UICollectionView {
    
    func empty(state: EmptyImageState) {
        let img = UIImageView()
        
        switch state {
        case .company:
            img.image = UIImage(named: "no-company")
        case .service:
            img.image = UIImage(named: "no-service")
        case .notif:
            img.image = UIImage(named: "no-service")
        case .history:
            break
        }
        
        
        img.contentMode = .scaleAspectFill
        img.translatesAutoresizingMaskIntoConstraints = false
        
        let lbl = UILabel()
        lbl.numberOfLines = 2
        lbl.textColor = .gray
        lbl.text = "No companies are providing \n this service at the moment"
        lbl.font = UIFont(name: "Montserrat-Medium", size: 16.0)
        lbl.frame = CGRect(origin: .zero, size: .init(width: 200, height: 80))
        lbl.translatesAutoresizingMaskIntoConstraints = false
        
        let chilStackView = UIStackView(arrangedSubviews: [img, lbl])
        chilStackView.axis = .vertical
        chilStackView.alignment = .center
        chilStackView.distribution = .equalSpacing
        
        chilStackView.spacing = 8
       
        let parentStackView = UIStackView(arrangedSubviews: [chilStackView])
        parentStackView.axis = .horizontal
        parentStackView.alignment = .center
     
        self.backgroundView = parentStackView
        
        img.widthAnchor.constraint(equalToConstant: 200).isActive = true
        img.heightAnchor.constraint(equalToConstant: 200).isActive = true
    }
}
