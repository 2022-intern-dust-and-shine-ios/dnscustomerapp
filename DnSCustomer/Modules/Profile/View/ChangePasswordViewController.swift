//
//  ChangePasswordViewController.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 3/8/22.
//

import UIKit

enum NotifFeatureFlag {
    static let isFlag = false
}

class ChangePasswordViewController: UIViewController, UNUserNotificationCenterDelegate {

    //MARK: - OUTLETS
    @IBOutlet weak var showhideeyeButton1: UIButton!
    @IBOutlet weak var showhideeyeButton2: UIButton!
    @IBOutlet weak var showhideweyeButton3: UIButton!
    
    @IBOutlet weak var oldpassTextfield: UITextField!
    @IBOutlet weak var newpassTextfield: UITextField!
    @IBOutlet weak var confirmpassTextfield: UITextField!
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var containerView1: UIView!
    @IBOutlet weak var containerView2: UIView!
    @IBOutlet weak var containerView3: UIView!
    @IBOutlet weak var changepassButton: UIButton! {
        didSet {
            changepassButton.setCornerRadius(8.0)
        }
    }
    @IBOutlet weak var cancelButton: UIButton! {
        didSet {
            cancelButton.setCornerRadius(8.0)
        }
    }
    //MARK: - PRESENTER
    private lazy var presenter = ProfilePresenter(delegate: self)
    
    private let alertController = IVAlertController()
    private let userDefault = UserDefaults.standard
    
    let userNotificationCenter = UNUserNotificationCenter.current()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.setTitle("Change Password")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTextfields()
        setupViews()
        alertController.dismiss = { [weak self] in
            guard let self = self else { return }
            DispatchQueue.main.async {
                self.redirectToSignIn()
            }
        }
        
        if NotifFeatureFlag.isFlag {
            userNotificationCenter.delegate = self
            requestNotificationAuthorization()
        }
    }

    func requestNotificationAuthorization() {
        let authOptions = UNAuthorizationOptions(arrayLiteral: .alert, .badge, .sound)
        
        userNotificationCenter.requestAuthorization(options: authOptions) { success, error in
            if let error = error {
                 print("Error: ", error)
             }
        }
    }
    private func redirectToSignIn() {
        guard let viewController = Modules.SignIn.initialViewController as? MyNavigationController else {
            return
        }
        viewController.modalPresentationStyle = .fullScreen
        present(viewController, animated: true, completion: nil)
    }
    
    private func reset() {
        DispatchQueue.main.async {
            self.userDefault.setToken(value: nil)
            //self.navigationController?.viewControllers.removeAll()
        }
    }
    
    func sendNotification() {
        let content = UNMutableNotificationContent()
        
        content.title = "Test"
        content.title = "This is body"
        content.badge = NSNumber(value: 3)
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5,
                                                        repeats: false)
        let request = UNNotificationRequest(identifier: "testNotification",
                                            content: content,
                                            trigger: trigger)
        
        userNotificationCenter.add(request) { error in
            if let error = error {
                  print("Notification Error: ", error)
              }
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        completionHandler()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .badge, .sound])
    }
  
    private func setupTextfields() {
        [oldpassTextfield,
         newpassTextfield,
         confirmpassTextfield].forEach { $0?.delegate = self;}
    }
    
    private func setupViews() {
        [containerView,
        containerView1,
        containerView2,
         containerView3].forEach { $0?.setCornerRadius(8.0) }
    }
    
    @IBAction func showhideAction(_ sender: UIButton) {
        let showPass = UIImage(systemName: "eye.fill")
        let hidePass = UIImage(systemName: "eye.slash.fill")
        switch sender.tag {
        case 1:
            if sender.isSelected {
                sender.isSelected = false
                oldpassTextfield.isSecureTextEntry = true
            } else {
                sender.isSelected = true
                oldpassTextfield.isSecureTextEntry = false
            }
          
            showhideeyeButton1.setImage(sender.isSelected ? showPass : hidePass, for: .normal)
        case 2:
            if sender.isSelected {
                sender.isSelected = false
                newpassTextfield.isSecureTextEntry = true
            } else {
                sender.isSelected = true
                newpassTextfield.isSecureTextEntry = false
            }
          
            showhideeyeButton2.setImage(sender.isSelected ? showPass : hidePass, for: .normal)
        case 3:
            if sender.isSelected {
                sender.isSelected = false
                confirmpassTextfield.isSecureTextEntry = true
            } else {
                sender.isSelected = true
                confirmpassTextfield.isSecureTextEntry = false
            }
            showhideweyeButton3.setImage(sender.isSelected ? showPass : hidePass, for: .normal)
        default:
            break
        }
    }
    @IBAction func cancelAction(_ sender: UIButton) {
        dismiss(animated: false)
    }
    @IBAction func saveAction(_ sender: UIButton) {
        sender.haptic()
        // Comment for now
        //sendNotification()
        print("PASS \(userDefault.getPassword())")
        let oldPass = oldpassTextfield.text!
        let newPass = newpassTextfield.text!
        let confirmPass = confirmpassTextfield.text!
        
        if oldPass.isNotEmpty() && newPass.isNotEmpty() && confirmPass.isNotEmpty() {
            if oldPass == userDefault.getPassword() {
                if newPass.count >= 8 && oldPass.count >= 8 {
                    if newPass == confirmPass {
                        presenter.changePassword(token: userDefault.getToken()!,
                                                 input: .init(oldPassword: oldPass,
                                                              password: newPass,
                                                              confirmPassword: confirmPass))
                    } else {
                        self.showAlert(message: "New password and confirm password didn't match")
                    }
                } else {
                    self.showAlert(message: "The password must be at least 8 characters and above")
                }

            } else {
                self.showAlert(message: "Old password didn't match")
            }
        
        } else {
            self.showAlert(message: "Invalid Input")
        }
    }
}

extension ChangePasswordViewController: ProfilePresenterDelegate {
    func showError(statusCode: Int) {
        self.showAlert(message: "Unable to process your request right now, Please try again later")
    }
    
    func showMessage(_ message: OutputChangePassword) {
        reset()
        alertController.showAlert(title: message.message, viewController: self.navigationController ?? self)
    }
}

extension ChangePasswordViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
