//
//  TermsAndConditionViewController.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 1/25/22.
//

import UIKit

class TermsAndConditionViewController: UIViewController {

    enum Constant {
        static let id = "TermsAndConditionViewController"
    }
    
    @IBOutlet weak var agreeButton: UIButton!
    @IBOutlet weak var containerView: UIView! {
        didSet {
            containerView.setCornerRadius(8)
        }
    }
    private let alertController = IVAlertController()
    var checkoutViewController: UIViewController?
    override func viewDidLoad() {
        super.viewDidLoad()
   
      
    }
    
    @IBAction func agreeAction(_ sender: UIButton) {
        if sender.isSelected {
            sender.isSelected = false
        } else {
            sender.isSelected = true
        }
        sender.isSelected ?
        agreeButton.setImage(UIImage(named: "check1"), for: .normal)
        :
        agreeButton.setImage(UIImage(named: "uncheck1"),for: .normal)
        
    }
    @IBAction func acceptAction(_ sender: UIButton) {
        dismiss(animated: false) {
//            self.alertController.showAlert(title: "Thank you, you have successfully booked!",
//                                           viewController: self.checkoutViewController!,
//                                           navController: (self.checkoutViewController?.navigationController)!)
        }
    }

}
