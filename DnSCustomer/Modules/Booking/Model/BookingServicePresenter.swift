//
//  BookServicePresenter.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 2/18/22.
//

import Foundation



protocol BookingServicePresenterDelegate: AnyObject {
    func showBookService(bookservice: OutputBookService)
    func showBookHistory(history: OutputHistory)
    func showError(_ error: String)
}

extension BookingServicePresenterDelegate {
    func showBookService(bookservice: OutputBookService) {}
    func showBookHistory(history: OutputHistory) {}
}

class BookingServicePresenter: NSObject {
    
    private weak var delegate: BookingServicePresenterDelegate?
    private let customerService = CustomerService()
    
    init(delegate: BookingServicePresenterDelegate) {
        self.delegate = delegate
    }
    
    public func getBookingService(token: String) {
        customerService.getBookingService(token) { [weak self] bookservice, statusCode, error in
            guard let self = self else { return }
            guard let bookservice = bookservice, error == nil else {
                self.delegate?.showError("Unable to process your request. Please try again")
                return }
            self.delegate?.showBookService(bookservice: bookservice)
        }
    }
    
    public func getBookingHistory(token: String) {
        customerService.getHistory(token) { [weak self] history, statusCode, error in
            guard let self = self else { return }
            guard let history = history, error == nil else {
                print(String(describing: error!))
                self.delegate?.showError("")
                return }
            self.delegate?.showBookHistory(history: history)
        }
    }
}
