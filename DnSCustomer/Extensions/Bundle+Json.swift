//
//  Bundle+json.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 1/24/22.
//

import Foundation


extension Bundle {
    
    func loadJsonResource(path: String) -> Data {
        
        guard let urlStringPath = self.path(forResource: path, ofType: "json") else {
            return Data()
        }
       
        do {
            
            let content = try String(contentsOfFile: urlStringPath)
        
            let data = Data(content.utf8)
            return data
        } catch let err as NSError {
            print(err.localizedDescription)
        }
        return Data()
    }
    
}
