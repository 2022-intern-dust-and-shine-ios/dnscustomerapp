//
//  ResetPasswordPresenter.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 3/31/22.
//

import Foundation


protocol ResetPasswordPresenterDelegate: AnyObject {
    func resetPassword(output: OutputResetPassword, statusCode: Int)
    
}

class ResetPasswordPresenter: NSObject {
    
    private weak var delegate: ResetPasswordPresenterDelegate?
    private let customerService = CustomerService()
    
    init(delegate: ResetPasswordPresenterDelegate) {
        self.delegate = delegate
    }
    
    public func postResetPassword(input: InputResetPassword) {
        customerService.postResetPassword(input: input) {
            [weak self] output, statusCode, error in
            guard let self = self else { return }
            guard let output = output, error == nil else { return }
            
            self.delegate?.resetPassword(output: output, statusCode: statusCode ?? 0)
        }
    }
}
