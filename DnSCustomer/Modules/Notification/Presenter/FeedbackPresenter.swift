//
//  FeedbackPresenter.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 3/7/22.
//

import Foundation

protocol FeedbackPresenterDelegate: AnyObject {
    func showFeedbackOutput(_ output: OutputReviews)
}

class FeedbackPresenter: NSObject {
    
    private let customerService = CustomerService()
    private weak var delegate: FeedbackPresenterDelegate?
    
    init(delegate: FeedbackPresenterDelegate) {
        self.delegate = delegate
    }
    
    public func postReview(token: String, input: InputReviews) {
        customerService.psotReview(token: token, input: input) {
            [weak self] reviews, statusCode, error in
            guard let self = self else { return }
            guard let reviews = reviews, error == nil else { return }
            self.delegate?.showFeedbackOutput(reviews)
        }
    }
}
