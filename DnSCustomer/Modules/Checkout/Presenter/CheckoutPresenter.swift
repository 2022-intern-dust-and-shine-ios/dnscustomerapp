//
//  CheckoutPresenter.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 2/16/22.
//

import Foundation



protocol CheckoutPresenterDelegate: AnyObject {
    func setBooking(booking: OutputCheckout)
    func showUserDetails(user: OutputUser)
    func showError(statusCode: Int)
}


class CheckoutPresenter: NSObject {
    
    private weak var delegate : CheckoutPresenterDelegate?
    private let customerService = CustomerService()
   
    init(delegate: CheckoutPresenterDelegate) {
        self.delegate = delegate
    }
    
    public func postBooking(_ input: InputCheckout, _ token: String) {
        customerService.createBooking(input, token) { [weak self] booking, statusCode, error in
            guard let self = self else { return }
            guard let booking = booking , error == nil else {
                print(String(describing: error))
                self.delegate?.showError(statusCode: statusCode ?? 0)
                return }
            self.delegate?.setBooking(booking: booking)
        }
    }
    public func getCustomerDetails(token: String) {
        customerService.getUser(token: token) { [weak self] user, statusCode, error in
            guard let self = self else { return }
            guard let user = user, error == nil else {
                self.delegate?.showError(statusCode: statusCode ?? 0)
                return }
            self.delegate?.showUserDetails(user: user)
        }
    }
}
