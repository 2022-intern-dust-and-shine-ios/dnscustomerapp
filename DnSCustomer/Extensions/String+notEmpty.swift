//
//  String+noEmpty.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 1/31/22.
//

import Foundation


extension String {
    func isNotEmpty() -> Bool {
        return !self.isEmpty
    }
}
