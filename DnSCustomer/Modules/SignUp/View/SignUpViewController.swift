//
//  SignUpViewController.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 1/14/22.
//

import UIKit

class SignUpViewController: UIViewController {

    //MARK: - OUTLETS
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var emailaddressTextField: UITextField!
    @IBOutlet weak var mobilenumberTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var repasswordTextField: UITextField!
    @IBOutlet weak var housenoTextField: UITextField!
    @IBOutlet weak var streetTextField: UITextField!
    @IBOutlet weak var barangayTextField: UITextField!
    @IBOutlet weak var citymunicipalityTextField: UITextField!
    @IBOutlet weak var provinceTextField: UITextField!
    @IBOutlet weak var zipcodeTextField: UITextField!
    
    @IBOutlet weak var firstnameContainerView: UIView!
    @IBOutlet weak var lastnameContainerView: UIView!
    @IBOutlet weak var emailaddressContainerView: UIView!
    @IBOutlet weak var mobilenumberContainerView: UIView!
    @IBOutlet weak var passwordContainerView: UIView!
    @IBOutlet weak var repasswordContainerView: UIView!
    @IBOutlet weak var housenoContainerView: UIView!
    @IBOutlet weak var streetContainerView: UIView!
    @IBOutlet weak var barangayContainerView: UIView!
    @IBOutlet weak var citymunicipalityContainerView: UIView!
    @IBOutlet weak var provinceContainerView: UIView!
    @IBOutlet weak var zipcodeContainerView: UIView!
    
    @IBOutlet weak var signupButton: UIButton!
    @IBOutlet weak var showPassButton: UIButton!
    @IBOutlet weak var showConfirPassButton: UIButton!
    
    @IBOutlet weak var constraintLayout: NSLayoutConstraint!
    @IBOutlet weak var showpassConstraint: NSLayoutConstraint!
    @IBOutlet weak var showconfpassConstraint: NSLayoutConstraint!
    
    //MARK: - PRESENTER
    private lazy var presenter = RegisterPresenter(delegate: self)
    
    //MARK: - SERVICE
    private lazy var keyboardService = KeyboardService(delegate: self)
    private lazy var toolbarService = ToolbarService(delegate: self)
    
    var alertController = IVAlertController()

    //MARK: - PROPERTIES
    var fname: String = ""
    
    let userDefault = UserDefaults.standard
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.setTitle("Create Account")
        self.navigationController?.navigationBar.setSyle(.white)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initViews()
        configureTextFields()
        
        dismissFromAlert()
        
        setupServices()
    }
 
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .darkContent
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        view.endEditing(true)
    }
    
    private func configureTextFields() {
        let fields = [firstNameTextField, lastNameTextField, emailaddressTextField,
                      mobilenumberTextField, passwordTextField, repasswordTextField,
                      housenoTextField, streetTextField, barangayTextField,
                      citymunicipalityTextField, provinceTextField, zipcodeTextField]
        fields.forEach{ $0?.delegate = self; $0?.tintColor = AppColor.blueColor }
    }
    
    private func initViews() {
        let val = CGFloat(10.0)
        let views = [firstnameContainerView, lastnameContainerView, emailaddressContainerView, mobilenumberContainerView,
                    passwordContainerView, repasswordContainerView, housenoContainerView, streetContainerView, barangayContainerView,
                    citymunicipalityContainerView, provinceContainerView, zipcodeContainerView]
        views.forEach{$0?.setCornerRadius(val)}
        signupButton.setCornerRadius(signupButton.frame.height / 2)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapView))
        self.view.addGestureRecognizer(tapGesture)
    }

    private func setupServices() {
        keyboardService.setupRegisterKbNotification()
        toolbarService.setupToolbar(textfields: [housenoTextField,
                                                zipcodeTextField,
                                                mobilenumberTextField])
    }
    private func dismissFromAlert() {
        alertController.dismiss = { [weak self] in
            guard let self = self else { return }
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    private func inputValidation(firstname: String?, lastname: String?,
                                 email: String?, number: String?,
                                 password: String?, repassword: String?,
                                 houseno: String?, street: String?,
                                 barangay: String?, city: String?,
                                 province: String?, zipcode: String?) -> Bool {
        
        if firstname != nil && firstname!.isNotEmpty() {
            if lastname != nil && lastname!.isNotEmpty() {
                if email != nil && email!.isNotEmpty() {
                    if number != nil && number!.isNotEmpty() {
                        if password != nil && password!.isNotEmpty() {
                            if repassword != nil && repassword!.isNotEmpty() {
                                if houseno != nil && houseno!.isNotEmpty() {
                                    if street != nil && street!.isNotEmpty() {
                                        if barangay != nil && barangay!.isNotEmpty() {
                                            if city != nil && city!.isNotEmpty() {
                                                if province != nil &&
                                                    province!.isNotEmpty() {
                                                    if zipcode != nil &&
                                                        zipcode!.isNotEmpty() {
                                                        if password == repassword {
                                                            if password!.count >= 8 {
                                                                return true
                                                            } else {
                                                                self.showAlert(message: "The password must be at least 8 characters and above")
                                                                return false
                                                            }
                                                            
                                                        } else {
                                                            
                                                            self.showAlert(message: "Password and confirmation password doesn't match")
                                                            return false
                                                        }
                                                    } else {
                                                    
                                                        self.showAlert(message: "Invalid zipcode")
                                                        zipcodeTextField.showError(zipcodeContainerView)
                                                        return false
                                                    }
                                                } else {
                                                    self.showAlert(message: "Invalid province")
                                                    provinceTextField.showError(provinceContainerView)
                                                    return false
                                                }
                                            } else {
                                                self.showAlert(message: "Invalid city")
                                                citymunicipalityTextField.showError(citymunicipalityContainerView)
                                                return false
                                            }
                                        } else {
                                            self.showAlert(message: "Invalid barangay")
                                            barangayTextField.showError(barangayContainerView)
                                            return false
                                        }
                                    } else {
                                        self.showAlert(message: "Invalid street")
                                        streetTextField.showError(streetContainerView)
                                        return true
                                    }
                                } else {
                                    self.showAlert(message: "Invalid house number")
                                    housenoTextField.showError(housenoContainerView)
                                    return false
                                }
                            } else {
                                self.showAlert(message: "Invalid confirm password")
                                repasswordTextField.showError(repasswordContainerView)
                                showConfirPassButton.isHidden = true
                                showconfpassConstraint.constant = 0
                                return false
                            }
                        } else {
                            self.showAlert(message: "Invalid password")
                            passwordTextField.showError(passwordContainerView)
                            showPassButton.isHidden = true
                            showpassConstraint.constant = 0
                            return false
                        }
                    } else {
                        self.showAlert(message: "Invalid mobile number")
                        mobilenumberTextField.showError(mobilenumberContainerView)
                        return false
                    }
                } else {
                    self.showAlert(message: "Invalid Email")
                    emailaddressTextField.showError(emailaddressContainerView)
                    return false
                }
            } else {
                self.showAlert(message: "Invalid Last name")
                lastNameTextField.showError(lastnameContainerView)
                return false
            }
        } else {
            self.showAlert(message: "Invalid First name")
            firstNameTextField.showError(firstnameContainerView)
            return false
        }
    }
    
    private func redirectToDashboard() {
        guard let viewController = Modules.Main.initialViewController as?
                UITabBarController else { return }
        viewController.modalPresentationStyle = .fullScreen
        self.present(viewController, animated: true, completion: nil)
    }
    
    @objc func tapView() {
        view.endEditing(true)
        constraintLayout.constant = 1
    }

    
    @IBAction func signUpAction(_ sender: UIButton) {
        sender.haptic()
        
        let firstName = firstNameTextField.text
        let lastName = lastNameTextField.text
        let email = emailaddressTextField.text
        let number = mobilenumberTextField.text
        let password = passwordTextField.text
        let repassword = repasswordTextField.text
        let houseno = housenoTextField.text
        let street = streetTextField.text
        let barangay = barangayTextField.text
        let city = citymunicipalityTextField.text
        let province = provinceTextField.text
        let zipcode = zipcodeTextField.text
        
        let isValid = inputValidation(firstname: firstName, lastname: lastName,
                                      email: email, number: number,
                                      password: password, repassword: repassword,
                                      houseno: houseno, street: street,
                                      barangay: barangay, city: city,
                                      province: province, zipcode: zipcode)
        
        if isValid {
            if Connectivity.isConnectedToInternet() {
                fname = firstName!
                userDefault.setPassword(value: password!)
                presenter.setRegister(input: .init(firstName: firstName!, lastName: lastName!,
                                            mobileNumber: number!, email: email!,
                                            password: password!, passwordConfirmation: repassword!,
                                            houseNumber: houseno!, street: street!,
                                            barangay: barangay!, municipality: city!,
                                            province: province!, zipcode: zipcode!))
            } else {
                self.showAlert(actionTitle: "Okay", message: "Please check your internet connection")
            }
        }
    }
    
    @IBAction func showPassAction(_ sender: UIButton) {
        let showPass = UIImage(systemName: "eye.fill")
        let hidePass = UIImage(systemName: "eye.slash.fill")
        if sender.isSelected {
            sender.isSelected = false
            passwordTextField.isSecureTextEntry = true
        } else {
            passwordTextField.isSecureTextEntry = false
            sender.isSelected = true
        }
        showPassButton.setImage(sender.isSelected ? showPass : hidePass,
                                for: .normal)
    }
    
    @IBAction func showConfiPassAction(_ sender: UIButton) {
        let showPass = UIImage(systemName: "eye.fill")
        let hidePass = UIImage(systemName: "eye.slash.fill")
        if sender.isSelected {
            sender.isSelected = false
            repasswordTextField.isSecureTextEntry = true
        } else {
            repasswordTextField.isSecureTextEntry = false
            sender.isSelected = true
        }
        showConfirPassButton.setImage(sender.isSelected ? showPass : hidePass,
                                for: .normal)
    }
}

extension SignUpViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        constraintLayout.constant = 1
        textField.resignFirstResponder()
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        switch textField.tag {
        case 1:
            textField.removeError(firstnameContainerView)
        case 2:
            textField.removeError(lastnameContainerView)
        case 3:
            textField.removeError(emailaddressContainerView)
        case 4:
            textField.removeError(mobilenumberContainerView)
        case 5:
            textField.removeError(passwordContainerView)
            showPassButton.isHidden = false
            showpassConstraint.constant = 27
        case 6:
            textField.removeError(repasswordContainerView)
            showConfirPassButton.isHidden = false
            showconfpassConstraint.constant = 27
        case 7:
            textField.removeError(housenoContainerView)
        case 8:
            textField.removeError(streetContainerView)
        case 9:
            textField.removeError(barangayContainerView)
        case 10:
            textField.removeError(citymunicipalityContainerView)
        case 11:
            textField.removeError(provinceContainerView)
        case 12:
            textField.removeError(zipcodeContainerView)
        default:
            break
        }
        return true
    }
}

extension SignUpViewController: RegisterPresenterDelegate {
    
    func presentOutputable(output: OutputRegister, _ statusCode: Int) {
        switch statusCode {
        case 200:
            userDefault.setFirstletter(value: String(fname.first ?? "A"))
            userDefault.setToken(value: output.accessToken)
            alertController.showAlert(title: "You have successfully registered",
                                      viewController: self.navigationController ?? self)
        case 422:
            self.showAlert(message: "The given data was invalid. The email has already been taken")
        default:
            break
        }
    }
    func presentError(_ error: String, _ statusCode: Int) {
        switch statusCode {
        case 422:
            self.showAlert(message: "The given data was invalid. The email has already been taken")
        case 500:
            self.showAlert(message: "The given data was invalid. Duplicate entry for house number")
        default:
            self.showAlert(message: "Unable to register. Something went wrong")
        }
    }
}

extension SignUpViewController: KeyboardProtocol {
    func getKeyboardSize(size: CGSize) {
        constraintLayout.constant = size.height
    }
}

extension SignUpViewController: ToolbarProtocol {
    func dismissKeyboard() {
        self.view.endEditing(true)
        constraintLayout.constant = 1
    }
}
