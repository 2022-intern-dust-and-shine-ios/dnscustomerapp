//
//  CompaniesAPI.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 2/10/22.
//

import Foundation
import Alamofire


enum CompanyAPI {
    case getCompany(_token: String, _ id: Int)
    case companies(_ token: String)
    case services(_ token: String)
    case filterCompaniesByService(_ token: String, _ id: Int)
    case search(_ keyword: String, _ token: String)
    case recommendation(_ token: String)
 
}

extension CompanyAPI: URLRequestConvertible, EndPointType {
    func asURLRequest() throws -> URLRequest {
        let url = baseURL.appendingPathComponent(path)
        var request = URLRequest(url: url)
        request.method = method
        request.headers = headers
        
        switch self {
        case .companies(let token),
                .services(let token),
                .search(_, let token),
                .getCompany(let token, _),
                .filterCompaniesByService(let token, _),
                .recommendation(let token):
            request = try URLEncodedFormParameterEncoder().encode(["token":token],
                                                                  into: request)

        }
        return request
    }
    
    var baseURL: URL {
        return URL(string: "https://lpn.boomtech.co/api")!
    }
    
    var path: String {
        switch self {
        case .companies:
            return "/companies"
        case .services:
            return "/services"
        case .search(let keyword, _):
            return "/search-company/\(keyword)"
        case .getCompany(_, let id):
            return "/companies/\(id)"
        case .filterCompaniesByService(_, let id):
            return "/filter-service/\(id)"
        case .recommendation:
            return "/recommendations"

        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .companies,
                .services,
                .search,
                .getCompany,
                .filterCompaniesByService,
                .recommendation:
            return .get

        }
    }
    
    var headers: HTTPHeaders {
        switch self {
        case .companies(let token),
                .services(let token),
                .search(_, let token),
                .getCompany(let token, _),
                .filterCompaniesByService(let token, _),
                .recommendation(let token):
            return [.accept("application/json"), .authorization(bearerToken: token)]
 
        }
    }
}
