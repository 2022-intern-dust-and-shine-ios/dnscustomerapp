//
//  AuthAPI.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 1/31/22.
//

import Foundation
import Alamofire


enum AuthAPI {
    case login(_ input: InputLogin)
    case register(_ input: InputRegister)
    case logout(_ token: String)

}

extension AuthAPI:  URLRequestConvertible, EndPointType {

    var baseURL: URL {
        return URL(string: "https://lpn.boomtech.co/api")!
    }
    
    var path: String {
        switch self {
        case .login:
            return "/login"
        case .register:
            return "/register"
        case .logout:
            return "/logout"

        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .login, .register, .logout:
            return .post

        }
    }
    
    var headers: HTTPHeaders {
        switch self {
        case .register, .login:
            return [.accept("application/json")]
        case .logout(let token):
            return [.accept("application/json"), .authorization(bearerToken: token)]
     
        }
    }
   
    func asURLRequest() throws -> URLRequest {
        let url = baseURL.appendingPathComponent(path)
        var request = URLRequest(url: url)
        request.method = method
        request.headers = headers
        
        switch self {
        case .login(let input):
            request = try JSONParameterEncoder().encode(input, into: request)
        case .register(let input):
            request = try JSONParameterEncoder().encode(input, into: request)
        case .logout(_):
            request = try URLEncodedFormParameterEncoder().encode(["":""], into: request)
  
        }
        return request
    }
}
