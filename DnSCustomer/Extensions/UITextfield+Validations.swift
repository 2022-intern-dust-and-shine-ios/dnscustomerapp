//
//  UITextfield+Validations.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 3/22/22.
//

import Foundation
import UIKit


extension UITextField {
    
    public func showError(_ containerView: UIView) {
        containerView.setCornerOutline(8.0, borderColor: .red)
        
        let errorImageView = UIImageView(frame: .zero)
        let errorImage = UIImage(systemName: "exclamationmark.triangle")
        errorImageView.tintColor = .red
        errorImageView.image = errorImage
        rightViewMode = .always
        rightView = errorImageView
    }
    public func removeError(_ containerView: UIView) {
        containerView.setCornerOutline(8.0, borderColor: .tertiarySystemGroupedBackground)
        
        rightViewMode = .never
        rightView = nil
    }
    
}
