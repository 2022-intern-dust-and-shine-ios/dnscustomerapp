//
//  UIView+ShowHide.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 2/28/22.
//

import Foundation
import UIKit


extension UIView {
    
    public func doHide() {
     
        UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.7, options: .curveEaseInOut, animations: {
            self.isHidden = true
            self.alpha = 0.0
        }, completion: nil)
    }
    public func doShow() {
        
        UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.7, options: .curveEaseInOut, animations: {
            self.isHidden = false
            self.alpha = 1.0
        }, completion: nil)
        
    }
}
