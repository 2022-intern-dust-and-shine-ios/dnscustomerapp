//
//  UINavigationController+StatusBarAppearance.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 1/14/22.
//

import Foundation
import UIKit

class MyNavigationController:  UINavigationController {
    
    open override func viewDidLoad() {
        print("NAV VC")
        if let _ = UserDefaults.standard.getToken() {
            print("NOT NIL TOKEN")
            guard let viewController = Modules.Main.initialViewController as?
                    UITabBarController else { return }
            present(viewController, animated: false, completion: nil)
        } else {
            print("NIL TOKEN")
//            guard let viewController = Modules.SignIn.initialViewController as?
//                    SignInViewController else { return }
//            present(viewController, animated: false, completion: nil)
        }
        
        self.setNeedsStatusBarAppearanceUpdate()
    }
    open override var preferredStatusBarStyle: UIStatusBarStyle {
        if let topVC = viewControllers.last {
            return topVC.preferredStatusBarStyle
        }
        return .default
    }
}
