//
//  RecommendedCompany.swift
//  DnSCustomer
//
//  Created by Ivan Dasigan on 3/11/22.
//

import Foundation



struct RecommendedCompany: Codable {
    var id: Int
    var companyId: Int
    var isActive: Int
    var parendId: Int
    var lft: Int
    var rgt: Int
    var depth: Int
    var createdAt: String
    var updatedAt: String
    var rating: Double
    var company: MCompany
    
    enum CodingKeys: String, CodingKey {
        case id, lft, rgt, depth, rating, company
        case isActive = "is_active"
        case companyId = "company_id"
        case parendId = "parent_id"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
}
